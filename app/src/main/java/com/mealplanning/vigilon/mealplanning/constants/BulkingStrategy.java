package com.mealplanning.vigilon.mealplanning.constants;

public class BulkingStrategy implements ITrainingStrategy
{
    @Override
    public int getStrategyIndex()
    {
        return StorageConstants.STRATEGY_BULKING;
    }

    @Override
    public float getProteinPercentage()
    {
        return 0.25f;
    }

    @Override
    public float getCarbohydratePercentage()
    {
        return 0.5f;
    }

    @Override
    public float getFatPercentage()
    {
        return 0.25f;
    }

    @Override
    public float getEnergyModifier() { return 1.1f; }
}
