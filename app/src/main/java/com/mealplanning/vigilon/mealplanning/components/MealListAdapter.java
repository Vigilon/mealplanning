package com.mealplanning.vigilon.mealplanning.components;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.dataStructures.MealVO;

import java.util.ArrayList;

public class MealListAdapter extends ArrayAdapter<MealVO>
{

    public MealListAdapter(Context context, int textViewResourceId, ArrayList<MealVO> meals)
    {
        super(context, textViewResourceId, meals);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent)
    {
        MealVO mealVO = getItem(position);

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.meal_list_item, parent,false);
        TextView textView = (TextView) view.findViewById(R.id.TF_mealListName);
        textView.setText("Food id: " + Integer.toString(mealVO.getIngredients().get(0).getFoodId()) + " - amount: " + Integer.toString(mealVO.getIngredients().get(0).getAmount()));

        TextView tf_carb = (TextView) view.findViewById(R.id.TF_carbohydrate);
        tf_carb.setText(getContext().getString(R.string.amount_carbohydrate, (int) mealVO.getTotalCarboyhdrateValue(), getContext().getString(R.string.unit_gram)));

        TextView tf_protein = (TextView) view.findViewById(R.id.TF_protein);
        tf_protein.setText(getContext().getString(R.string.amount_protein, (int) mealVO.getTotalProteinValue(), getContext().getString(R.string.unit_gram)));

        TextView tf_fat = (TextView) view.findViewById(R.id.TF_fat);
        tf_fat.setText(getContext().getString(R.string.amount_fat, (int) mealVO.getTotalFatValue(), getContext().getString(R.string.unit_gram)));
        return view;
    }
}
