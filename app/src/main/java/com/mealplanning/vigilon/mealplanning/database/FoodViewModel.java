package com.mealplanning.vigilon.mealplanning.database;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;

import com.mealplanning.vigilon.mealplanning.database.entities.Food;
import com.mealplanning.vigilon.mealplanning.database.repositories.FoodRepository;

import java.util.List;

public class FoodViewModel extends AndroidViewModel
{
    private FoodRepository mRepository;

    public FoodViewModel(Application application)
    {
        super(application);
        mRepository = new FoodRepository(application);
    }

    public void insert(Food food)
    {
        mRepository.insert(food);
    }

    public Food getFoodById(int id)
    {
        return mRepository.getFoodById(id);
    }

    public Food getFoodByName(String name)
    {
        return mRepository.getFoodByName(name);
    }

    public List<Food> getFoodWithNameLike(String name)
    {
        return mRepository.getFoodWithNameLike(name);
    }

    public List<String> getAllFoodNames()
    {
        return mRepository.getAllFoodNames();
    }
}
