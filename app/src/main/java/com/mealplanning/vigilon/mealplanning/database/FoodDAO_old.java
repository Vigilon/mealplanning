package com.mealplanning.vigilon.mealplanning.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mealplanning.vigilon.mealplanning.dataStructures.FoodVO;

import static com.mealplanning.vigilon.mealplanning.database.DatabaseHelper.DB_COLUMN_CARB;
import static com.mealplanning.vigilon.mealplanning.database.DatabaseHelper.DB_COLUMN_CARB_ENERGY_RATIO;
import static com.mealplanning.vigilon.mealplanning.database.DatabaseHelper.DB_COLUMN_FAT;
import static com.mealplanning.vigilon.mealplanning.database.DatabaseHelper.DB_COLUMN_FAT_ENERGY_RATIO;
import static com.mealplanning.vigilon.mealplanning.database.DatabaseHelper.DB_COLUMN_FOOD_NAME;
import static com.mealplanning.vigilon.mealplanning.database.DatabaseHelper.DB_COLUMN_ID;
import static com.mealplanning.vigilon.mealplanning.database.DatabaseHelper.DB_COLUMN_IS_LIQUID;
import static com.mealplanning.vigilon.mealplanning.database.DatabaseHelper.DB_COLUMN_PROTEIN;
import static com.mealplanning.vigilon.mealplanning.database.DatabaseHelper.DB_COLUMN_PROTEIN_ENERGY_RATIO;
import static com.mealplanning.vigilon.mealplanning.database.DatabaseHelper.DB_COLUMN_TOTAL_ENERGY;
import static com.mealplanning.vigilon.mealplanning.database.DatabaseHelper.DB_COLUMN_SALT;

/**
 * this is connector class that holds reference to the database and is responsible for data retrieval
 */
public class FoodDAO_old
{

    private SQLiteDatabase database;
    private String[] allColumns = {DatabaseHelper.DB_COLUMN_ID, DB_COLUMN_FOOD_NAME, DB_COLUMN_CARB, DB_COLUMN_PROTEIN, DB_COLUMN_FAT, DB_COLUMN_SALT, DB_COLUMN_IS_LIQUID};

    public FoodDAO_old(Context context)
    {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(context);
        database = dbHelper.getDatabase();
    }

    public void addFood(FoodVO food)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DB_COLUMN_FOOD_NAME, food.getName());
        contentValues.put(DB_COLUMN_CARB, food.getCarbohydrateValue());
        contentValues.put(DB_COLUMN_PROTEIN, food.getProteinValue());
        contentValues.put(DB_COLUMN_FAT, food.getFatValue());
        contentValues.put(DB_COLUMN_SALT, food.getSaltValue());
        contentValues.put(DB_COLUMN_IS_LIQUID, food.getIsLiquid() ? 1 : 0);

        // the following information is for search purposes only and will be recreated once retrieving macronutrients from the db
        contentValues.put(DB_COLUMN_TOTAL_ENERGY, food.getEnergyValue());
        contentValues.put(DB_COLUMN_CARB_ENERGY_RATIO, food.getCarbohydrateEnergyRatio());
        contentValues.put(DB_COLUMN_PROTEIN_ENERGY_RATIO, food.getProteinEnergyRatio());
        contentValues.put(DB_COLUMN_FAT_ENERGY_RATIO, food.getFatEnergyRatio());

        Log.i(this.toString() + " - insertFood", "Inserting: " + food.getName());
        database.insert(DatabaseHelper.DB_TABLE_FOOD, null, contentValues);
    }

    public void updateFood(FoodVO food)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DB_COLUMN_FOOD_NAME, food.getName());
        contentValues.put(DB_COLUMN_CARB, food.getCarbohydrateValue());
        contentValues.put(DB_COLUMN_PROTEIN, food.getProteinValue());
        contentValues.put(DB_COLUMN_FAT, food.getFatValue());
        contentValues.put(DB_COLUMN_SALT, food.getSaltValue());
        contentValues.put(DB_COLUMN_IS_LIQUID, food.getIsLiquid() ? 1 : 0);

        // the following information is for search purposes only and will be recreated once retrieving macronutrients from the db
        contentValues.put(DB_COLUMN_TOTAL_ENERGY, food.getEnergyValue());
        contentValues.put(DB_COLUMN_CARB_ENERGY_RATIO, food.getCarbohydrateEnergyRatio());
        contentValues.put(DB_COLUMN_PROTEIN_ENERGY_RATIO, food.getProteinEnergyRatio());
        contentValues.put(DB_COLUMN_FAT_ENERGY_RATIO, food.getFatEnergyRatio());
        database.update(DatabaseHelper.DB_TABLE_FOOD, contentValues, DB_COLUMN_ID + " = " + Integer.toString(food.getId()), null);
    }

    public FoodVO getFoodByName(String foodName)
    {
        Cursor cursor = database.query(DatabaseHelper.DB_TABLE_FOOD,
                allColumns, DB_COLUMN_FOOD_NAME + " = '" + foodName + "'", null,
                null, null, null);
        cursor.moveToFirst();
        FoodVO food = cursorToFood(cursor);
        cursor.close();
        return food;
    }

    public FoodVO getFoodById(int foodId)
    {
        Cursor cursor = database.query(DatabaseHelper.DB_TABLE_FOOD,
                allColumns, DB_COLUMN_ID + " = '" + foodId + "'", null,
                null, null, null);
        cursor.moveToFirst();
        FoodVO food = cursorToFood(cursor);
        cursor.close();
        return food;
    }

    /**
     * forms a food data object with a given cursor
     *
     * @param cursor database cursor
     * @return FoodVO
     */
    private FoodVO cursorToFood(Cursor cursor)
    {
        FoodVO food = new FoodVO();
        food.setId(cursor.getInt(0));
        food.setName(cursor.getString(1));
        food.setMacronutrients(cursor.getFloat(2), cursor.getFloat(3), cursor.getFloat(4));
        food.setSaltValue(cursor.getFloat(5));
        food.setIsLiquid(cursor.getInt(6) == 1);
        return food;
    }
}
