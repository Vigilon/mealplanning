package com.mealplanning.vigilon.mealplanning.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.database.entities.Food;
import com.mealplanning.vigilon.mealplanning.database.FoodViewModel;
import com.mealplanning.vigilon.mealplanning.widgets.MacroDistributionChart;
import com.mealplanning.vigilon.mealplanning.widgets.SearchableAdapter;

import java.util.List;

public class SelectFoodFragment extends Fragment
{
    private FoodViewModel foodViewModel;

    private AutoCompleteTextView autoCompleteTextView;
    //private ArrayAdapter<String> autoCompleteAdapter;
    private SearchableAdapter autoCompleteAdapter;
    private ConstraintLayout infoContainer;

    private LinearLayout propertyLayoutContainer;

    private TextView nutritionValueHeadlineText;
    private TextView energyValueText;
    private TextView carbohydrateValueText;
    private TextView proteinValueText;
    private TextView fatValueText;
    private TextView saltValueText;

    private MacroDistributionChart pieChart;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_show_food, container, false);
        autoCompleteTextView = view.findViewById(R.id.ACTV_select_food);
        infoContainer = view.findViewById(R.id.CL_show_food_container);
        infoContainer.setVisibility(View.GONE);

        propertyLayoutContainer = view.findViewById(R.id.LL_select_food_icon_container);
        nutritionValueHeadlineText = view.findViewById(R.id.TV_select_food_values_label);

        energyValueText = view.findViewById(R.id.TV_select_food_energy);
        carbohydrateValueText = view.findViewById(R.id.TV_select_food_carb);
        proteinValueText = view.findViewById(R.id.TV_select_food_protein);
        fatValueText = view.findViewById(R.id.TV_select_food_fat);
        saltValueText = view.findViewById(R.id.TV_select_food_salt);

        pieChart = view.findViewById(R.id.pieChart);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        foodViewModel = ViewModelProviders.of(this).get(FoodViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        // get a list with names of all foods stored
        List<String> foodNames = foodViewModel.getAllFoodNames();

        //autoCompleteAdapter = new ArrayAdapter<>(view.getContext(), R.layout.list_item);
        //autoCompleteAdapter.addAll(foodNames);
        autoCompleteAdapter = new SearchableAdapter(view.getContext(), foodNames);
        autoCompleteTextView.setAdapter(autoCompleteAdapter);
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id)
            {
                // determine selected food name
                String itemName = (String) parent.getItemAtPosition(pos);

                // grab full food information and show
                showSelectedFood(foodViewModel.getFoodByName(itemName));

                // hide soft keyboard when item was chosen
                InputMethodManager in = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (in != null)
                {
                    in.hideSoftInputFromWindow(arg1.getApplicationWindowToken(), 0);
                }
            }
        });
    }

    private void showSelectedFood(Food food)
    {
        if (food != null)
        {
            infoContainer.setVisibility(View.VISIBLE);

            // delete all old views
            propertyLayoutContainer.removeAllViews();

            // fill with new icons
            if (food.getVegan())
            {
                ImageView imageView = new ImageView(getContext());
                imageView.setImageResource(R.drawable.ic_vegetarian);
                imageView.setTooltipText("Vegan");
                propertyLayoutContainer.addView(imageView);
            }
            if (food.getLiquid())
            {
                ImageView imageView = new ImageView(getContext());
                imageView.setImageResource(R.drawable.ic_liquid);
                imageView.setTooltipText("Liquid");
                propertyLayoutContainer.addView(imageView);
            }

            if (food.getLiquid())
            {
                nutritionValueHeadlineText.setText(getString(R.string.placeholder_nutrition_per_unit, 100, getString(R.string.unit_milliliter)));
            }
            else
            {
                nutritionValueHeadlineText.setText(getString(R.string.placeholder_nutrition_per_unit, 100, getString(R.string.unit_gram)));
            }

            // fill table
            energyValueText.setText(getString(R.string.placeholder_calories, (int)(food.getEnergyValue() * 100)));
            carbohydrateValueText.setText(getString(R.string.placeholder_gram, food.getCarbohydrateValue() * 100));
            proteinValueText.setText(getString(R.string.placeholder_gram, food.getProteinValue() * 100));
            fatValueText.setText(getString(R.string.placeholder_gram, food.getFatValue() * 100));
            saltValueText.setText(getString(R.string.placeholder_gram, food.getSaltValue() * 100));

            pieChart.setMacros(food.getCarbohydrateEnergyRatio(), food.getProteinEnergyRatio(), food.getFatEnergyRatio(), (int)(food.getEnergyValue()*100));
        }
        else
        {
            Log.d("Show food: ", "FOOD NOT FOUND");
        }
    }
}
