package com.mealplanning.vigilon.mealplanning.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.mealplanning.vigilon.mealplanning.R;

public class SlideActivity extends Fragment
{
    private Bundle userValues;

    private SeekBar activityBar;
    private TextView activityDescription;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_slide_activity, container, false);

        activityDescription = (TextView) v.findViewById(R.id.TF_slideActivity);
        activityBar = (SeekBar) v.findViewById(R.id.SB_activity);

        // define content of fragment
        userValues = getArguments().getBundle(FragmentSetupConstants.BUNDLE_NAME);
        int activityIndex = FragmentSetupConstants.DEFAULT_ACTIVITY;
        if (userValues != null)
        {
            activityIndex = userValues.getInt(FragmentSetupConstants.BUNDLE_IDENTIFIER_ACTIVITY, FragmentSetupConstants.DEFAULT_ACTIVITY);
        }

        activityBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b)
            {
                updateActivityDisplay(i);
                setActivityIndex(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar)
            {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {

            }
        });

        activityBar.setMax(0);
        activityBar.setMax(3);
        activityBar.setProgress(activityIndex);
        activityBar.refreshDrawableState();

        updateActivityDisplay(activityIndex);

        return v;
    }

    public static SlideActivity newInstance(Bundle userValues)
    {
        SlideActivity f = new SlideActivity();
        Bundle b = new Bundle();
        b.putBundle(FragmentSetupConstants.BUNDLE_NAME, userValues);
        f.setArguments(b);

        return f;
    }

    protected void updateActivityDisplay(int activtityIndex)
    {
        switch (activtityIndex)
        {
            case 0:
                // no training
                activityDescription.setText(getString(R.string.activityRate_noActivity));
                break;
            case 1:
                // light training (one to three hours a week)
                //setActivityIndex(1.2f);
                activityDescription.setText(getString(R.string.activityRate, 1, 3));
                break;
            case 2:
                // moderate training (four to 6 hours a week)
                //setActivityIndex(1.35f);
                activityDescription.setText(getString(R.string.activityRate, 4, 6));
                break;
            case 3:
                // active training (more than 6 hours a week)
                //setActivityIndex(1.5f);
                activityDescription.setText(getString(R.string.activateRate_high, 6));
                break;
            default:
                // something is wrong here
        }
    }

    protected void setActivityIndex(int newValue)
    {
        userValues.putInt(FragmentSetupConstants.BUNDLE_IDENTIFIER_ACTIVITY, newValue);
    }
}
