package com.mealplanning.vigilon.mealplanning;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.mealplanning.vigilon.mealplanning.components.WearableMessageSenderManager;
import com.mealplanning.vigilon.mealplanning.constants.StorageConstants;
import com.mealplanning.vigilon.mealplanning.dataStructures.FoodVO;
import com.mealplanning.vigilon.mealplanning.dataStructures.IngredientVO;
import com.mealplanning.vigilon.mealplanning.dataStructures.MealVO;
import com.mealplanning.vigilon.mealplanning.dataStructures.UserVO;
import com.mealplanning.vigilon.mealplanning.database.DatabaseHelper;
import com.mealplanning.vigilon.mealplanning.dataStructures.DayVO;
import com.mealplanning.vigilon.mealplanning.database.DayDAO;
import com.mealplanning.vigilon.mealplanning.database.FoodDAO_old;
import com.mealplanning.vigilon.mealplanning.database.UserDAO;
import com.mealplanning.vigilon.mealplanning.fragments.DailyOverviewFragment;
import com.mealplanning.vigilon.mealplanning.fragments.RecipeListFragment;
import com.mealplanning.vigilon.mealplanning.fragments.SelectFoodFragment;

public class MealPlanning extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
{

    protected SharedPreferences preferences;

    // day state object
    private DayVO day;
    private UserVO user;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // init layout, tool bar and side bars
        setContentView(R.layout.activity_meal_planning);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // sync default shared preferences with settings
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        // add default push notification channel
        WearableMessageSenderManager.getInstance().createNotificationChannel(this, WearableMessageSenderManager.BASE_CHANNEL, "Push Notification", "Channel for Push Notifications");

        preferences = this.getSharedPreferences(StorageConstants.PREFERENCE_FILE, Context.MODE_PRIVATE);

        // open database connection for app start - create database if needed
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(this);
        dbHelper.openDB();

        // fill database with test data
        initDatabase();

        // loads/sets up user data and loads/sets up the current day
        initUserData();

        if (savedInstanceState == null)
        {
            DailyOverviewFragment fragment = new DailyOverviewFragment();
            fragment.setDayVO(day);
            getSupportFragmentManager().beginTransaction().add(R.id.content_meal_planning, fragment).commit();
        }
    }

    /**
     * creates food database entries
     */
    protected void initDatabase()
    {
        boolean isAppInitialized = preferences.getBoolean(StorageConstants.APP_INITIALIZED, false);
        if (!isAppInitialized)
        {
            // TODO get external data input
            // set food entries in database

            FoodDAO_old foodDAOOld = new FoodDAO_old(this);

            FoodVO carb = new FoodVO();
            carb.setName("Kohlenhydrate");
            carb.setMacronutrients(1,0,0);
            foodDAOOld.addFood(carb);

            FoodVO protein = new FoodVO();
            protein.setName("Protein");
            protein.setMacronutrients(0,1,0);
            foodDAOOld.addFood(protein);

            FoodVO fat = new FoodVO();
            fat.setName("Fett");
            fat.setMacronutrients(0,0,1);
            foodDAOOld.addFood(fat);

            FoodVO milk = new FoodVO();
            milk.setName("Milch (1,5%)");
            milk.setMacronutrients(0.05f, 0.034f, 0.015f);
            milk.setSaltValue(0.044f);
            milk.setIsLiquid(true);
            foodDAOOld.addFood(milk);

            FoodVO milk2 = new FoodVO();
            milk2.setName("Milch (3,5%)");
            milk2.setMacronutrients(0.048f, 0.033f, 0.035f);
            milk2.setSaltValue(0.0013f);
            milk2.setIsLiquid(true);
            foodDAOOld.addFood(milk2);

            FoodVO magerquark = new FoodVO();
            magerquark.setName("Magerquark");
            magerquark.setMacronutrients(0.041f, 0.12f, 0.002f);
            magerquark.setSaltValue(0.001f);
            foodDAOOld.addFood(magerquark);

            FoodVO banane = new FoodVO();
            banane.setName("Banane");
            banane.setMacronutrients(0.2f, 0.01f, 0.002f);
            foodDAOOld.addFood(banane);

            FoodVO hahnchen = new FoodVO();
            hahnchen.setName("Haehnchen");
            hahnchen.setMacronutrients(0f, 0.223f, 0.015f);
            foodDAOOld.addFood(hahnchen);

            FoodVO ananasTK = new FoodVO();
            ananasTK.setName("Ananas (TK)");
            ananasTK.setMacronutrients(0.08f, 0.004f, 0.002f);
            foodDAOOld.addFood(ananasTK);

            FoodVO kirschenTK = new FoodVO();
            kirschenTK.setName("Kirschen (TK)");
            kirschenTK.setMacronutrients(0.136f, 0.008f, 0.001f);
            kirschenTK.setSaltValue(0f);
            foodDAOOld.addFood(kirschenTK);

            FoodVO chiasamen = new FoodVO();
            chiasamen.setName("Chiasamen");
            chiasamen.setMacronutrients(0.018f, 0.22f, 0.33f);
            chiasamen.setSaltValue(0f);
            foodDAOOld.addFood(chiasamen);

            FoodVO nutella = new FoodVO();
            nutella.setName("Nutella");
            nutella.setMacronutrients(0.575f, 0.063f, 0.309f);
            nutella.setSaltValue(0.00107f);
            nutella.setIsLiquid(false);
            foodDAOOld.addFood(nutella);

            FoodVO mate = new FoodVO();
            mate.setName("Mate");
            mate.setMacronutrients(0.052f, 0, 0);
            mate.setSaltValue(0f);
            mate.setIsLiquid(true);
            foodDAOOld.addFood(mate);

            FoodVO haferflocken = new FoodVO();
            haferflocken.setName("Haferflocken");
            haferflocken.setMacronutrients(0.587f, 0.135f, 0.07f );
            haferflocken.setSaltValue(0.00017f);
            haferflocken.setIsLiquid(false);
            foodDAOOld.addFood(haferflocken);

            // mark app as initialized
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(StorageConstants.APP_INITIALIZED, true);
            editor.apply();
        }
    }

    /**
     * get user data, if there is one, create new user via sub activity
     */
    protected void initUserData()
    {
        int activeUserId = preferences.getInt(StorageConstants.ACTIVE_USER_ID, -1);

        // if a user is already present, continue with day setup
        if (loadUser(activeUserId))
        {
            loadDay();
        }
    }

    /**
     * get the current day data, create a new day if current does not exist
     */
    protected void loadDay()
    {
        DayDAO dayDAO = new DayDAO(this);
        day = dayDAO.getCurrentDay();

        // if there is no day, create a new one with values based on the current user
        if (day == null)
        {
            day = new DayVO();
            day.initWithUser(user);
            dayDAO.addDay(day);
        }
        // if there is already a day, check if limit
        else
        {
            day.updateWithUser(user);
            dayDAO.updateDay(day);
        }
    }

    protected boolean loadUser(int userID)
    {
        UserDAO userDao = new UserDAO(this);
        user = userDao.getUserById(userID);

        if (user == null)
        {
            showUserSetupDialog();
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.meal_planning, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id)
        {
            case R.id.action_settings:
                showDayOverviewDialog();
                return true;
            case R.id.action_setup:
                // show small dialog to enter weight, body fat etc
                showUserSetupDialog();
                return true;
            case R.id.action_recipe:
                getSupportFragmentManager().beginTransaction().replace(R.id.content_meal_planning, new RecipeListFragment()).addToBackStack(null).commit();
                return true;
            default:
                // option does not exist somehow
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id)
        {
            case R.id.nav_camera:
                addMeal(null);
                break;
            case R.id.nav_planning:
                // open calendar
                break;
            case R.id.nav_settings:
                // open settings screen
                Intent i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                break;
            default:
                // nothing to handle :(
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showUserSetupDialog()
    {
        Intent intent = new Intent(this, UserSetupActivity.class);
        startActivityForResult(intent, StorageConstants.SUB_ACTIVITY_USER_SETUP);
    }

    public void showDayOverviewDialog()
    {
        Intent intent = new Intent(this, DayOverviewActivity.class);
        startActivityForResult(intent, StorageConstants.SUB_ACTIVITY_DAY_OVERVIEW);
    }

    public void showAddMealDialog()
    {
        //Intent intent = new Intent(this, AddMealActivity.class);
        //startActivityForResult(intent, StorageConstants.SUB_ACTIVITY_ADD_MEAL);
        getSupportFragmentManager().beginTransaction().replace(R.id.content_meal_planning, new SelectFoodFragment()).addToBackStack(null).commit();
    }

    // handle return from a sub activity
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode != RESULT_CANCELED)
        {
            switch (requestCode)
            {
                case StorageConstants.SUB_ACTIVITY_USER_SETUP:
                    int newUserId = data.getIntExtra(StorageConstants.NEW_USER_ID, -1);
                    if (newUserId != -1)
                    {
                        // mark new user as active one
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putInt(StorageConstants.ACTIVE_USER_ID, newUserId);
                        editor.apply();

                        initUserData();
                    }
                    else
                    {
                        // something really went wrong here
                    }
                    break;
                case StorageConstants.SUB_ACTIVITY_ADD_MEAL:
                    int foodId = data.getIntExtra("foodId", -1);
                    if (foodId != -1)
                    {
                        int amount = data.getIntExtra("amount", 0);
                        MealVO meal = new MealVO();
                        meal.addIngredient(new IngredientVO(foodId, amount));
                        day.addMealToList(meal);

                        DayDAO dayDAO = new DayDAO(this);
                        Boolean succesful = dayDAO.updateDay(day);
                        if (succesful)
                        {
                            //updateDailyMacronutrients();
                        }
                    }
                    break;
                default:
                    // unknown sub activity
            }
        }
    }

    /**
     * Called when the user touches the button
     */
    public void addMeal(View view)
    {
        // Do something in response to button click
        showAddMealDialog();
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }
}