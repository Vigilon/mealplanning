package com.mealplanning.vigilon.mealplanning.constants;

public class StorageConstants
{
    public static final String PREFERENCE_FILE = "preferenceFile";
    public static final String APP_INITIALIZED = "appInitialized";
    public static final String ACTIVE_USER_ID = "activeUserID";

    public static final String PREFERENCE_KEY_PUSH_NOTIFICATION = "pref_push_notification";

    public static final int SUB_ACTIVITY_USER_SETUP = 1;
    public static final int SUB_ACTIVITY_ADD_MEAL = 2;
    public static final int SUB_ACTIVITY_DAY_OVERVIEW = 3;


    public static final int STRATEGY_BULKING = 1;
    public static final int STRATEGY_CUTTING = 2;
    public static final int STRATEGY_SUSTAIN = 3;

    public static final String NEW_USER_ID = "newUserId";

    public static final String CURRENT_TDEE = "currentTDEE";
    public static final String CURRENT_DAILY_PROTEIN_AMOUNT = "currentDailyProteinAmount";
    public static final String CURRENT_DAILY_PROTEIN_GUIDE_NUMBER = "currentDailyProteinGuideNumber";
    public static final String CURRENT_DAILY_CARBOHYDRATE_AMOUNT = "currentDailyCarbohydrateAmount";
    public static final String CURRENT_DAILY_CARBOHYDRATE_GUIDE_NUMBER = "currentDailyCarbohydrateGuideNumber";
    public static final String CURRENT_DAILY_FAT_AMOUNT = "currentDailyFatAmount";
    public static final String CURRENT_DAILY_FAT_GUIDE_NUMBER = "currentDailyFatGuideNumber";
}
