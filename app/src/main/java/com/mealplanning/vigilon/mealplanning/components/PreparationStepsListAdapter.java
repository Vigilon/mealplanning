package com.mealplanning.vigilon.mealplanning.components;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mealplanning.vigilon.mealplanning.R;

import java.util.List;

public class PreparationStepsListAdapter extends RecyclerView.Adapter<PreparationStepsListAdapter.IngredientListViewHolder>
{
    private List<String> preparationSteps;

    public class IngredientListViewHolder extends RecyclerView.ViewHolder
    {
        public TextView stepNumber, stepDescription;

        public IngredientListViewHolder(View view)
        {
            super(view);
            stepNumber = view.findViewById(R.id.TV_preparation_step_list_item_number);
            stepDescription = view.findViewById(R.id.TV_preparation_step_list_item_description);
        }
    }

    public PreparationStepsListAdapter(List<String> preparationSteps)
    {
        this.preparationSteps = preparationSteps;
    }

    @Override
    @NonNull
    public PreparationStepsListAdapter.IngredientListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.preparation_step_list_item, parent, false);

        return new PreparationStepsListAdapter.IngredientListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PreparationStepsListAdapter.IngredientListViewHolder holder, int position)
    {
        String preparationStep = preparationSteps.get(position);
        holder.stepNumber.setText((position + 1) + ")");
        holder.stepDescription.setText(preparationStep);
    }

    @Override
    public int getItemCount()
    {
        if (preparationSteps == null) {
            return 0;
        } else {
            return preparationSteps.size();
        }
    }
}