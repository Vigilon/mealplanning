package com.mealplanning.vigilon.mealplanning.database.repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.mealplanning.vigilon.mealplanning.database.MealPlanningDatabase;
import com.mealplanning.vigilon.mealplanning.database.daos.FoodDAO;
import com.mealplanning.vigilon.mealplanning.database.entities.Food;

import java.util.ArrayList;
import java.util.List;

public class FoodRepository
{
    private FoodDAO mFoodDao;
    private LiveData<List<Food>> mAllFoods;

    public FoodRepository(Application application)
    {
        MealPlanningDatabase db = MealPlanningDatabase.getDatabase(application);
        mFoodDao = db.foodDAO();
        mAllFoods = mFoodDao.getAllFoods();
    }

    public void insert(Food food)
    {
        new insertAsyncTask(mFoodDao).execute(food);
    }

    private static class insertAsyncTask extends AsyncTask<Food, Void, Void>
    {
        private FoodDAO mAsyncTaskDao;

        insertAsyncTask(FoodDAO dao)
        {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Food... params)
        {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public Food getFoodById(int id)
    {
        Food food;
        try
        {
            food = new selectFoodAsyncTask(mFoodDao).execute(id).get();
        } catch (Exception e)
        {
            food = null;
        }
        return food;
    }

    private static class selectFoodAsyncTask extends AsyncTask<Integer, Void, Food>
    {
        private FoodDAO mAsyncTaskDao;

        selectFoodAsyncTask(FoodDAO dao)
        {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Food doInBackground(final Integer... params)
        {
            return mAsyncTaskDao.getFoodById(params[0]);
        }
    }

    public Food getFoodByName(String name)
    {
        Food food;
        try
        {
            food = new selectFoodByNameAsyncTask(mFoodDao).execute(name).get();
        } catch (Exception e)
        {
            food = null;
        }
        return food;
    }

    private static class selectFoodByNameAsyncTask extends AsyncTask<String, Void, Food>
    {
        private FoodDAO mAsyncTaskDao;

        selectFoodByNameAsyncTask(FoodDAO dao)
        {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Food doInBackground(final String... params)
        {
            return mAsyncTaskDao.getFoodByName(params[0]);
        }
    }

    public List<String> getAllFoodNames()
    {
        List<String> foodNames;
        try
        {
            foodNames = new getAllFoodNamesAsyncTask(mFoodDao).execute().get();
        } catch (Exception e)
        {
            foodNames = new ArrayList<>();
        }
        return foodNames;
    }

    private static class getAllFoodNamesAsyncTask extends AsyncTask<Void, Void, List<String>>
    {
        private FoodDAO mAsyncTaskDao;

        getAllFoodNamesAsyncTask(FoodDAO dao)
        {
            mAsyncTaskDao = dao;
        }

        @Override
        protected List<String> doInBackground(Void... params)
        {
            return mAsyncTaskDao.getAllFoodNames();
        }
    }

    public List<Food> getFoodWithNameLike(String name)
    {
        List<Food> foodList;
        try
        {
            foodList = new selectFoodWithNameLikeAsyncTask(mFoodDao).execute(name).get();
        } catch (Exception e)
        {
            foodList = null;
        }
        return foodList;
    }

    private static class selectFoodWithNameLikeAsyncTask extends AsyncTask<String, Void, List<Food>>
    {
        private FoodDAO mAsyncTaskDao;

        selectFoodWithNameLikeAsyncTask(FoodDAO dao)
        {
            mAsyncTaskDao = dao;
        }

        @Override
        protected List<Food> doInBackground(final String... params)
        {
            return new ArrayList<>();
            //return mAsyncTaskDao.getFoodWithNameLike(params[0]);
        }
    }
}
