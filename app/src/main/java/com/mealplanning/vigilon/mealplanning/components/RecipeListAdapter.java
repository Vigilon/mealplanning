package com.mealplanning.vigilon.mealplanning.components;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.database.entities.Recipe;

import java.util.List;

public class RecipeListAdapter extends RecyclerView.Adapter<RecipeListAdapter.RecipeListViewHolder>
{
    private List<Recipe> mRecipes;
    private LayoutInflater mLayoutInflater;
    private View.OnClickListener onClickListener;

    public class RecipeListViewHolder extends RecyclerView.ViewHolder
    {
        private TextView recipeName, recipePlaceholder;
        private CheckBox favorite;

        private RecipeListViewHolder(View view)
        {
            super(view);
            recipeName = view.findViewById(R.id.TV_recipe_list_item_name);
            recipePlaceholder = view.findViewById(R.id.TV_recipe_list_item_placheholder);

            favorite = view.findViewById(R.id.CB_recipe_list_item_favorite);
        }
    }

    public RecipeListAdapter(Context context)
    {
        mLayoutInflater = LayoutInflater.from(context);
    }

    public void setClickListener(View.OnClickListener callback)
    {
        onClickListener = callback;
    }

    @Override
    public RecipeListViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = mLayoutInflater.inflate(R.layout.recipe_list_item, parent, false);
        itemView.setOnClickListener(onClickListener);

        return new RecipeListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeListViewHolder holder, int position)
    {
        if (mRecipes != null)
        {
            Recipe recipe = mRecipes.get(position);
            holder.recipeName.setText(recipe.getTitle());
            holder.recipePlaceholder.setText("Item: " + position);
            holder.favorite.setChecked(recipe.isFavorite());
            holder.itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    onClickListener.onClick(v);
                }
            });
        }
        else {
            holder.recipeName.setText("Recipe not loaded yet");
        }

    }

    public void setRecipes(List<Recipe> recipes){
        mRecipes = recipes;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount()
    {
        if (mRecipes != null)
        {
            return mRecipes.size();
        }
        else
        {
            return 0;
        }
    }
}
