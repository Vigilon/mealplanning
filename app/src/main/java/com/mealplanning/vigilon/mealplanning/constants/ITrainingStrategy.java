package com.mealplanning.vigilon.mealplanning.constants;

public interface ITrainingStrategy
{
    int getStrategyIndex();

    float getProteinPercentage();

    float getCarbohydratePercentage();

    float getFatPercentage();

    float getEnergyModifier();
}
