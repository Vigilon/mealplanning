package com.mealplanning.vigilon.mealplanning.database;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.util.Pair;

import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.dataStructures.IngredientVO;
import com.mealplanning.vigilon.mealplanning.database.entities.Food;
import com.mealplanning.vigilon.mealplanning.database.entities.Recipe;
import com.mealplanning.vigilon.mealplanning.database.repositories.FoodRepository;
import com.mealplanning.vigilon.mealplanning.database.repositories.RecipeRepository;

import java.util.ArrayList;
import java.util.List;

// NEVER PASS CONTEXT TO A VIEW MODEL!

public class RecipeViewModel extends AndroidViewModel
{
    private RecipeRepository mRepository;
    private FoodRepository mFoodRepository;
    private LiveData<List<Recipe>>  mAllRecipes;

    public RecipeViewModel(Application application) {
        super(application);
        mRepository = new RecipeRepository(application);
        mFoodRepository = new FoodRepository(application);
        mAllRecipes = mRepository.getAllRecipes();
    }

    public LiveData<List<Recipe>> getAllRecipes()
    {
        return mAllRecipes;
    }

    public Recipe getRecipeById(int id)
    {
        return mRepository.getRecipeById(id);
    }

    public void insert(Recipe recipe) { mRepository.insert(recipe); }

    public void toggleFavorite(int id) {

    }

    public void addToFavorites(int recipeId)
    {
        mRepository.setFavoriteStateForRecipe(recipeId, true);
    }

    public void removeFromFavorites(int recipeId)
    {
        mRepository.setFavoriteStateForRecipe(recipeId, false);
    }

    public List<Pair<String, String>> getIngredientListForRecipeId(int recipeId) {
        Recipe recipe = getRecipeById(recipeId);
        List<IngredientVO> ingredients = recipe.getIngredients();
        List<Pair<String, String>> readableList = new ArrayList<>();
        if (ingredients != null) {
            for (IngredientVO ingredient : ingredients)
            {
                Food food = mFoodRepository.getFoodById(ingredient.getFoodId());
                String unit = food.getLiquid() ? getApplication().getString(R.string.unit_milliliter) : getApplication().getString(R.string.unit_gram);
                String amount = Integer.toString(ingredient.getAmount()) + " " + unit;
                String name = food.getName();

                readableList.add(new Pair<>(amount, name));
            }
        }
        return readableList;
    }
}
