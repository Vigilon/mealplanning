package com.mealplanning.vigilon.mealplanning.constants;


public class CuttingStrategy implements ITrainingStrategy
{
    public int getStrategyIndex()
    {
        return StorageConstants.STRATEGY_CUTTING;
    }

    @Override
    public float getProteinPercentage()
    {
        return 0.4f;
    }

    @Override
    public float getCarbohydratePercentage()
    {
        return 0.4f;
    }

    @Override
    public float getFatPercentage()
    {
        return 0.2f;
    }

    @Override
    public float getEnergyModifier()
    {
        return 0.8f;
    }

    // cutting strategy conservative: 10-20%
    // cutting strategy moderate: 21-30%
    // cutting strategy aggressive: 31-40%
}
