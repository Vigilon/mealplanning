package com.mealplanning.vigilon.mealplanning.fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.widgets.ButtonNumberPicker;

public class SlideBodyWeight extends Fragment
{
    private Bundle userValues;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_slide_body_weight, container, false);

        float inputValue = FragmentSetupConstants.DEFAULT_WEIGHT;

        userValues = getArguments().getBundle(FragmentSetupConstants.BUNDLE_NAME);
        if (userValues != null)
        {
            inputValue = userValues.getFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_WEIGHT, FragmentSetupConstants.DEFAULT_WEIGHT);
        }

        ButtonNumberPicker weightPicker = (ButtonNumberPicker) v.findViewById(R.id.BTNP_weight);
        weightPicker.setInputValue(inputValue);
        weightPicker.setOnChangeListener(new ButtonNumberPicker.ButtonNumberPickerListener()
        {
            @Override
            public void onValueChanged(float value)
            {
                userValues.putFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_WEIGHT, value);
            }
        });

        return v;
    }

    public static SlideBodyWeight newInstance(Bundle userValues)
    {
        SlideBodyWeight f = new SlideBodyWeight();
        Bundle b = new Bundle();
        b.putBundle(FragmentSetupConstants.BUNDLE_NAME, userValues);
        f.setArguments(b);

        return f;
    }
}
