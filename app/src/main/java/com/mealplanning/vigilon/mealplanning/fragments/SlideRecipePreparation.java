package com.mealplanning.vigilon.mealplanning.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.components.PreparationStepsListAdapter;
import com.mealplanning.vigilon.mealplanning.database.RecipeViewModel;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SlideRecipePreparation extends Fragment
{
    private static final String RECIPE_ID = "recipeId";

    private TextView recipePreparationTime;

    private RecyclerView preparationStepListView;
    private List<String> preparationStepList;

    private RecipeViewModel recipeViewModel;

    public SlideRecipePreparation()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        recipeViewModel = ViewModelProviders.of(this).get(RecipeViewModel.class);

        if (getArguments() != null)
        {
            int recipeId = getArguments().getInt(RECIPE_ID);
            preparationStepList = recipeViewModel.getRecipeById(recipeId).getPreparationSteps();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_slide_recipe_preparation, container, false);
        recipePreparationTime = view.findViewById(R.id.TV_recipe_preparation_time);

        preparationStepListView = view.findViewById(R.id.RV_recipe_preparation_steps_list);
        preparationStepListView.setHasFixedSize(true);
        preparationStepListView.setNestedScrollingEnabled(false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(preparationStepListView.getContext(), layoutManager.getOrientation());

        preparationStepListView.setLayoutManager(layoutManager);
        preparationStepListView.addItemDecoration(itemDecoration);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        recipePreparationTime.setText(getString(R.string.placeholder_preparation_time, 5));
        PreparationStepsListAdapter ingredientListAdapter = new PreparationStepsListAdapter(preparationStepList);
        preparationStepListView.setAdapter(ingredientListAdapter);

        ingredientListAdapter.notifyDataSetChanged();

    }

    public static SlideRecipePreparation newInstance(int recipeId)
    {
        SlideRecipePreparation fragment = new SlideRecipePreparation();
        Bundle args = new Bundle();
        args.putInt(RECIPE_ID, recipeId);
        fragment.setArguments(args);
        return fragment;
    }
}
