package com.mealplanning.vigilon.mealplanning.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.widgets.ButtonNumberPicker;

public class SlideAge extends Fragment
{
    private Bundle userValues;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_slide_age, container, false);

        float inputValue = FragmentSetupConstants.DEFAULT_AGE;

        // define content of fragment
        userValues = getArguments().getBundle(FragmentSetupConstants.BUNDLE_NAME);
        if (userValues != null)
        {
            inputValue = (float) userValues.getInt(FragmentSetupConstants.BUNDLE_IDENTIFIER_AGE, FragmentSetupConstants.DEFAULT_AGE);
        }

        ButtonNumberPicker agePicker = (ButtonNumberPicker) v.findViewById(R.id.BTNP_age);
        agePicker.setInputValue(inputValue);
        agePicker.setOnChangeListener(new ButtonNumberPicker.ButtonNumberPickerListener()
        {
            @Override
            public void onValueChanged(float value)
            {
                userValues.putFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_AGE, value);
            }
        });

        return v;
    }

    public static SlideAge newInstance(Bundle userValues)
    {
        SlideAge f = new SlideAge();
        Bundle b = new Bundle();
        b.putBundle(FragmentSetupConstants.BUNDLE_NAME, userValues);
        f.setArguments(b);

        return f;
    }
}
