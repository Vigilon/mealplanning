package com.mealplanning.vigilon.mealplanning.dataStructures;

import android.media.Image;

import java.util.List;

public class RecipeVO
{
    private int id;

    private String name;

    private String description;

    private Image image;

    private String preparationDescription;

    // time for preparation in minutes
    private int preparationTime;

    private List<IngredientVO> ingredients;

    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Image getImage()
    {
        return image;
    }

    public void setImage(Image image)
    {
        this.image = image;
    }

    public int getPreparationTime()
    {
        return preparationTime;
    }

    public void setPreparationTime(int preparationTime)
    {
        this.preparationTime = preparationTime;
    }

    public List<IngredientVO> getIngredients()
    {
        return ingredients;
    }

    public void setIngredients(List<IngredientVO> ingredients)
    {
        this.ingredients = ingredients;
    }

    public String getPreparationDescription()
    {
        return preparationDescription;
    }

    public void setPreparationDescription(String preparationDescription)
    {
        this.preparationDescription = preparationDescription;
    }
}
