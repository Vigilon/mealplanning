package com.mealplanning.vigilon.mealplanning.components;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class TouchImageButton extends android.support.v7.widget.AppCompatImageButton
{
    private Handler buttonHandler;
    private int startDelay = 500;

    // consecutive button calls
    private int intervals = 0;

    // interval to the first consecutive button calls in ms
    private int triggerIntervalStart = 200;

    // time reduction between consecutive button calls in ms
    private int triggerIntervalReduction = 10;

    // minimum interval between button calls in ms
    private int triggerIntervalMin = 100;

    Runnable buttonAction = new Runnable()
    {
        @Override
        public void run()
        {
            performClick();

            int timeUntilNextCall = Math.min( triggerIntervalMin, triggerIntervalStart - (intervals * triggerIntervalReduction));
            buttonHandler.postDelayed(this, timeUntilNextCall);

            // increase interval counter
            intervals++;
        }
    };

    public TouchImageButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                performClick();
                if (buttonHandler != null) return true;
                buttonHandler = new Handler();
                buttonHandler.postDelayed(buttonAction, startDelay);

                // reset interval counter, button callback should start slow again
                intervals = 0;
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                if (buttonHandler == null) return true;
                buttonHandler.removeCallbacks(buttonAction);
                buttonHandler = null;
        }
        return true;
    }

    @Override
    public boolean performClick()
    {
        return super.performClick();
    }
}
