package com.mealplanning.vigilon.mealplanning.components;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.mealplanning.vigilon.mealplanning.MealPlanning;
import com.mealplanning.vigilon.mealplanning.R;

public class WearableMessageSenderManager
{
    public static final String BASE_CHANNEL = "mealPlanningChannel";

    private WearableMessageSenderManager()
    {
    }

    private static volatile WearableMessageSenderManager instance;

    public static WearableMessageSenderManager getInstance()
    {
        if  (instance == null) {
            synchronized (WearableMessageSenderManager.class) {
                if (instance == null) {
                    instance = new WearableMessageSenderManager();
                }
            }
        }

        return instance;
    }


    public void sendMessage(Context context, String message)
    {
        int notificationId = 6418;
        // Build intent for notification content
        Intent viewIntent = new Intent(context, MealPlanning.class);
        viewIntent.putExtra("EXTRA_EVENT_ID", 5);
        PendingIntent viewPendingIntent = PendingIntent.getActivity(context, 1, viewIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context, BASE_CHANNEL)
                        .setSmallIcon(R.drawable.menu_calendar)
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentText(message)
                        .setContentIntent(viewPendingIntent);

        // Get an instance of the NotificationManager service
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(context);

        // Issue the notification with notification manager.
        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    public void createNotificationChannel(Context context, String channelId, String channelName, String channelDescription)
    {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);
            channel.setDescription(channelDescription);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            if (notificationManager != null)
            {
                notificationManager.createNotificationChannel(channel);
            }
        }
    }

}
