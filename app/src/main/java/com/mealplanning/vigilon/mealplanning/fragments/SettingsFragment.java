package com.mealplanning.vigilon.mealplanning.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;

import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.components.WearableMessageSenderManager;
import com.mealplanning.vigilon.mealplanning.constants.StorageConstants;

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener
{
    private Context context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onPause() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
    {
        if (key.equals(StorageConstants.PREFERENCE_KEY_PUSH_NOTIFICATION))
        {
            if (sharedPreferences.getBoolean(StorageConstants.PREFERENCE_KEY_PUSH_NOTIFICATION, false) && context != null)
            {
                WearableMessageSenderManager.getInstance().sendMessage(context, "Push Notifications enabled");
            }
        }
    }
}
