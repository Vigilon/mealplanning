package com.mealplanning.vigilon.mealplanning.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.components.TouchImageButton;

import java.util.Locale;

public class ButtonNumberPicker extends LinearLayout
{
    public interface ButtonNumberPickerListener
    {
        void onValueChanged(float value);
    }

    protected TouchImageButton btnPlus;
    protected TouchImageButton btnMinus;
    protected EditText txtInput;

    protected float minValue = 0f;
    protected float maxValue = 100f;
    protected float stepValue = 1f;

    protected float inputValue = 50f;

    protected ButtonNumberPickerListener listener;

    public ButtonNumberPicker(Context context)
    {
        super(context);
        init(context, null);
    }

    public ButtonNumberPicker(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context, attrs);
    }

    public ButtonNumberPicker(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    protected void init(Context context, AttributeSet attrs)
    {
        LayoutInflater.from(context).inflate(R.layout.button_number_picker, this, true);

        setOrientation(LinearLayout.HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);

        txtInput = (EditText) findViewById(R.id.ET_NumberPickerInput);
        txtInput.setOnEditorActionListener(new EditText.OnEditorActionListener()
                                {
                                    @Override
                                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
                                    {
                                        if (actionId == EditorInfo.IME_ACTION_DONE)
                                        {
                                            float newValue = Float.parseFloat(v.getText().toString());
                                            inputValue = Math.min(Math.max(minValue, newValue), maxValue);
                                            onInputValueUpdated();
                                        }
                                        return false;
                                    }
                                }
        );

        btnPlus = (TouchImageButton) findViewById(R.id.BTN_NumberPickerPlus);
        btnPlus.setOnClickListener(new TouchImageButton.OnClickListener()
                                   {
                                       public void onClick(View v)
                                       {
                                           onPlusButtonClicked();
                                       }
                                   }
        );

        btnMinus = (TouchImageButton) findViewById(R.id.BTN_NumberPickerMinus);
        btnMinus.setOnClickListener(new TouchImageButton.OnClickListener()
                                    {
                                        public void onClick(View v)
                                        {
                                            onMinusButtonClicked();
                                        }
                                    }
        );

        // set xml style attribute values
        if (attrs != null)
        {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ButtonNumberPicker);
            minValue = a.getFloat(R.styleable.ButtonNumberPicker_minValue, 0f);
            maxValue = a.getFloat(R.styleable.ButtonNumberPicker_maxValue, 100f);
            stepValue = a.getFloat(R.styleable.ButtonNumberPicker_stepValue, 1f);
            a.recycle();
        }
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
    }

    protected void onPlusButtonClicked()
    {
        if (inputValue < maxValue)
        {
            inputValue = Math.min(inputValue + stepValue, maxValue);
            onInputValueUpdated();
        }
    }

    protected void onMinusButtonClicked()
    {
        if (inputValue > minValue)
        {
            inputValue = Math.max(inputValue - stepValue, minValue);
            onInputValueUpdated();
        }
    }

    public void setInputValue(float value)
    {
        inputValue = value;
        onInputValueUpdated();
    }

    protected void onInputValueUpdated()
    {
        txtInput.setText(String.format(Locale.US, "%.1f", inputValue));

        if (listener != null)
        {
            listener.onValueChanged(inputValue);
        }
    }

    public void setOnChangeListener(ButtonNumberPickerListener listener)
    {
        this.listener = listener;
    }
}
