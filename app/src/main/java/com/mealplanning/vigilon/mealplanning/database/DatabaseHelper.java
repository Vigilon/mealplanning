package com.mealplanning.vigilon.mealplanning.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DatabaseHelper extends SQLiteOpenHelper {
    // only keep one instance of the helper
    private static DatabaseHelper dbInstance;

    private static final String DB_NAME = "mealplanning.db";
    private static final int DB_VERSION_NUMBER = 1;
    static final String DB_TABLE_FOOD = "food";
    static final String DB_TABLE_USER = "user";
    static final String DB_TABLE_DAY = "days";
    static final String DB_TABLE_RECIPE = "recipes";

    // food table
    static final String DB_COLUMN_ID = "_ID";
    static final String DB_COLUMN_FOOD_NAME = "food_name";
    static final String DB_COLUMN_CARB = "carbohydrate";
    static final String DB_COLUMN_PROTEIN = "protein";
    static final String DB_COLUMN_FAT = "fat";
    static final String DB_COLUMN_SALT = "salt";
    static final String DB_COLUMN_IS_LIQUID = "isLiquid";
    static final String DB_COLUMN_TOTAL_ENERGY = "energy";
    static final String DB_COLUMN_CARB_ENERGY_RATIO = "cer";
    static final String DB_COLUMN_PROTEIN_ENERGY_RATIO = "per";
    static final String DB_COLUMN_FAT_ENERGY_RATIO = "fer";

    // user table
    static final String DB_COLUMN_USER_NAME = "user_name";
    static final String DB_COLUMN_BODY_WEIGHT = "body_weight";
    static final String DB_COLUMN_BODY_HEIGHT = "body_height";
    static final String DB_COLUMN_BODY_FAT = "body_fat";
    static final String DB_COLUMN_AGE = "age";
    static final String DB_COLUMN_GENDER = "gender";
    static final String DB_COLUMN_ACTIVITY = "activity_level";
    static final String DB_COLUMN_STRATEGY = "strategy";

    // day table
    static final String DB_COLUMN_DATE = "date";
    static final String DB_COLUMN_CARB_LIMIT = "carb_limit";
    static final String DB_COLUMN_PROTEIN_LIMIT = "protein_limit";
    static final String DB_COLUMN_FAT_LIMIT = "fat_limit";
    static final String DB_COLUMN_ENERGY_LIMIT = "energy_limit";
    static final String DB_COLUMN_MEAL_LIST = "meal_list";

    // recipe table
    static final String DB_COLUMN_RECIPE_NAME = "recipe_name";
    static final String DB_COLUMN_RECIPE_DESCRIPTION = "description";
    static final String DB_COLUMN_RECIPE_PREP_DESCRIPTION = "preparation_description";
    static final String DB_COLUMN_RECIPE_PREP_DURATION = "preparation_duration";
    static final String DB_COLUMN_RECIPE_IMAGE = "image";
    static final String DB_COLUMN_RECIPE_INGREDIENT_LIST = "ingredients";

    private static final String DB_CREATE_FOOD_TABLE_SCRIPT = "create table if not exists " + DB_TABLE_FOOD +
            " (" + DB_COLUMN_ID + " integer primary key autoincrement, "
            + DB_COLUMN_FOOD_NAME + " text not null, "
            + DB_COLUMN_CARB + " REAL DEFAULT 0, "
            + DB_COLUMN_PROTEIN + " REAL DEFAULT 0, "
            + DB_COLUMN_FAT + " REAL DEFAULT 0, "
            + DB_COLUMN_SALT + " REAL DEFAULT 0, "
            + DB_COLUMN_IS_LIQUID + " INTEGER DEFAULT 0,"
            + DB_COLUMN_TOTAL_ENERGY + " REAL DEFAULT 0, "
            + DB_COLUMN_CARB_ENERGY_RATIO + " REAL DEFAULT 0, "
            + DB_COLUMN_PROTEIN_ENERGY_RATIO + " REAL DEFAULT 0, "
            + DB_COLUMN_FAT_ENERGY_RATIO + " REAL DEFAULT 0"
            + ");)";

    private static final String DB_CREATE_USER_TABLE_SCRIPT = "create table if not exists " + DB_TABLE_USER +
            " (" + DB_COLUMN_ID + " integer primary key autoincrement, "
            + DB_COLUMN_USER_NAME + " text not null, "
            + DB_COLUMN_BODY_WEIGHT + " REAL DEFAULT 0, "
            + DB_COLUMN_BODY_HEIGHT + " REAL DEFAULT 0, "
            + DB_COLUMN_BODY_FAT + " REAL DEFAULT 0, "
            + DB_COLUMN_AGE + " INTEGER DEFAULT 0, "
            + DB_COLUMN_GENDER + " INTEGER DEFAULT 0, "
            + DB_COLUMN_ACTIVITY + " INTEGER DEFAULT 0, "
            + DB_COLUMN_STRATEGY + " INTEGER DEFAULT 0"
            + ");)";

    private static final String DB_CREATE_DAY_TABLE_SCRIPT = "create table if not exists " + DB_TABLE_DAY
            + " (" + DB_COLUMN_DATE + " TEXT NOT NULL PRIMARY KEY, "
            + DB_COLUMN_CARB_LIMIT + " REAL DEFAULT 0, "
            + DB_COLUMN_PROTEIN_LIMIT + " REAL DEFAULT 0, "
            + DB_COLUMN_FAT_LIMIT + " REAL DEFAULT 0, "
            + DB_COLUMN_ENERGY_LIMIT + " REAL DEFAULT 0, "
            + DB_COLUMN_MEAL_LIST + " TEXT DEFAULT '[]'"
            + ");)";

    private static final String DB_CREATE_RECIPE_TABLE_SCRIPT = "CREATE TABLE IF NOT EXISTS " + DB_TABLE_RECIPE
            + " (" + DB_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + DB_COLUMN_RECIPE_NAME + " TEXT NOT NULL, "
            + DB_COLUMN_RECIPE_DESCRIPTION + " TEXT, "
            + DB_COLUMN_RECIPE_PREP_DESCRIPTION + " TEXT NOT NULL, "
            + DB_COLUMN_RECIPE_PREP_DURATION + " INTEGER DEFAULT 0, "
            + DB_COLUMN_RECIPE_INGREDIENT_LIST + " TEXT NOT NULL, "
            + DB_COLUMN_RECIPE_IMAGE + " BLOB"
            + ");)";

    private SQLiteDatabase sqliteDBInstance = null;

    // TODO Check PreparedStatements for SQLite commands

    public static synchronized DatabaseHelper getInstance(Context context) {
        if (dbInstance == null) {
            dbInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return dbInstance;
    }

    private DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION_NUMBER);
    }

    public SQLiteDatabase getDatabase() throws SQLException {
        if (sqliteDBInstance == null)
        {
            sqliteDBInstance = dbInstance.getWritableDatabase();
        }
        return sqliteDBInstance;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO: Implement onUpgrade
    }

    @Override
    public void onCreate(SQLiteDatabase sqliteDBInstance) {
        Log.i("onCreate", "Creating the database...");
        sqliteDBInstance.execSQL(DB_CREATE_USER_TABLE_SCRIPT);
        sqliteDBInstance.execSQL(DB_CREATE_FOOD_TABLE_SCRIPT);
        sqliteDBInstance.execSQL(DB_CREATE_DAY_TABLE_SCRIPT);
        sqliteDBInstance.execSQL(DB_CREATE_RECIPE_TABLE_SCRIPT);
    }

    public void openDB() throws SQLException {
        Log.i("openDB", "Checking sqliteDBInstance...");
        if (this.sqliteDBInstance == null) {
            Log.i("openDB", "Creating sqliteDBInstance...");
            this.sqliteDBInstance = this.getWritableDatabase();
        }
    }

    public void closeDB() {
        if (this.sqliteDBInstance != null) {
            if (this.sqliteDBInstance.isOpen()) {
                this.sqliteDBInstance.close();
            }
        }
    }

    public String[] getAllFood() {
        Cursor cursor = this.sqliteDBInstance.query(DB_TABLE_FOOD, new String[]{DB_COLUMN_FOOD_NAME}, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            String[] str = new String[cursor.getCount()];
            int i = 0;

            while (cursor.moveToNext()) {
                str[i] = cursor.getString(cursor.getColumnIndex(DB_COLUMN_FOOD_NAME));
                i++;
            }
            return str;
        } else {
            return new String[]{};
        }
    }
}
