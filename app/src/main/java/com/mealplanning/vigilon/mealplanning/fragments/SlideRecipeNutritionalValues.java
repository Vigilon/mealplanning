package com.mealplanning.vigilon.mealplanning.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.dataStructures.RecipeVO;

/**
 * A simple {@link Fragment} subclass.
 */
public class SlideRecipeNutritionalValues extends Fragment
{
    private static final String PROTEIN_VALUE = "proteinValue";
    private static final String CARBOHYDRATE_VALUE = "carbohydrateValue";
    private static final String FAT_VALUE = "fatValue";

    public SlideRecipeNutritionalValues()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_slide_recipe_nutritional_values, container, false);
    }

    public static SlideRecipeNutritionalValues newInstance(RecipeVO recipeVO)
    {
        SlideRecipeNutritionalValues fragment = new SlideRecipeNutritionalValues();
        Bundle args = new Bundle();

        args.putInt(PROTEIN_VALUE, 5);
        args.putInt(CARBOHYDRATE_VALUE, 10);
        args.putInt(FAT_VALUE, 2);

        fragment.setArguments(args);
        return fragment;
    }

}
