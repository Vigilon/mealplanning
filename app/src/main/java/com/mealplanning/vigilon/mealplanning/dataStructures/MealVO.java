package com.mealplanning.vigilon.mealplanning.dataStructures;

import com.mealplanning.vigilon.mealplanning.database.FoodDAO_old;

import java.util.LinkedList;
import java.util.List;

public class MealVO
{
    private List<IngredientVO> _ingredients;

    private float _totalCarboyhdrateValue;
    private float _totalProteinValue;
    private float _totalFatValue;

    public MealVO()
    {
        _ingredients = new LinkedList<>();
    }

    public List<IngredientVO> getIngredients()
    {
        return _ingredients;
    }

    public void addIngredient( IngredientVO ingredient )
    {
        _ingredients.add(ingredient);

        // get details about the ingredient
        FoodDAO_old foodDAOOld = new FoodDAO_old(null);
        FoodVO foodVO = foodDAOOld.getFoodById(ingredient.getFoodId());

        // add macronutrients to the meal
        _totalCarboyhdrateValue += foodVO.getCarbohydrateValue() * ingredient.getAmount();
        _totalProteinValue += foodVO.getProteinValue() * ingredient.getAmount();
        _totalFatValue += foodVO.getFatValue() * ingredient.getAmount();
    }

    public float getTotalCarboyhdrateValue() { return _totalCarboyhdrateValue; }

    public float getTotalProteinValue() { return _totalProteinValue; }

    public float getTotalFatValue() { return _totalFatValue; }
}
