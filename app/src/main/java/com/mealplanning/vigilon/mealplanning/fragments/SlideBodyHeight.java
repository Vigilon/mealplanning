package com.mealplanning.vigilon.mealplanning.fragments;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.widgets.ButtonNumberPicker;

public class SlideBodyHeight extends Fragment
{
    private Bundle userValues;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_slide_body_height, container, false);

        float inputValue = FragmentSetupConstants.DEFAULT_HEIGHT;

        // define content of fragment
        userValues = getArguments().getBundle(FragmentSetupConstants.BUNDLE_NAME);
        if (userValues != null)
        {
            inputValue = userValues.getFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_HEIGHT, FragmentSetupConstants.DEFAULT_HEIGHT);
        }

        ButtonNumberPicker heightPicker = (ButtonNumberPicker) v.findViewById(R.id.BTNP_height);
        heightPicker.setInputValue(inputValue);
        heightPicker.setOnChangeListener(new ButtonNumberPicker.ButtonNumberPickerListener()
        {
            @Override
            public void onValueChanged(float value)
            {
                userValues.putFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_HEIGHT, value);
            }
        });

        return v;
    }

    public static SlideBodyHeight newInstance(Bundle userValues)
    {
        SlideBodyHeight f = new SlideBodyHeight();
        Bundle b = new Bundle();
        b.putBundle(FragmentSetupConstants.BUNDLE_NAME, userValues);
        f.setArguments(b);

        return f;
    }
}
