package com.mealplanning.vigilon.mealplanning;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.mealplanning.vigilon.mealplanning.constants.StorageConstants;
import com.mealplanning.vigilon.mealplanning.dataStructures.UserVO;
import com.mealplanning.vigilon.mealplanning.database.UserDAO;
import com.mealplanning.vigilon.mealplanning.fragments.FragmentSetupConstants;
import com.mealplanning.vigilon.mealplanning.fragments.SlideActivity;
import com.mealplanning.vigilon.mealplanning.fragments.SlideAge;
import com.mealplanning.vigilon.mealplanning.fragments.SlideBodyFat;
import com.mealplanning.vigilon.mealplanning.fragments.SlideBodyHeight;
import com.mealplanning.vigilon.mealplanning.fragments.SlideBodyWeight;
import com.mealplanning.vigilon.mealplanning.fragments.SlideGender;
import com.mealplanning.vigilon.mealplanning.fragments.SlideResultFragment;
import com.mealplanning.vigilon.mealplanning.fragments.SlideStrategy;
import com.mealplanning.vigilon.mealplanning.fragments.SlideWelcome;

public class UserSetupActivity extends AppCompatActivity
{
    // pager instance managing all slides
    ViewPager pager;

    // bundle with stored user values to ensure data exchange between slides
    Bundle userValues;

    private UserDAO userDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_setup);

        // init db connection object
        userDAO = new UserDAO(this);

        // init bundle first
        userValues = initDataBundle();

        pager = (ViewPager) findViewById(R.id.viewPager);
        pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        pager.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

            }
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabDots);
        tabLayout.setupWithViewPager(pager, true);
    }

    protected Bundle initDataBundle()
    {
        // read out last active user shared preferences (not set on app start)
        SharedPreferences preferences = this.getSharedPreferences(StorageConstants.PREFERENCE_FILE, Context.MODE_PRIVATE);
        int activeUserId = preferences.getInt(StorageConstants.ACTIVE_USER_ID, -1);

        if (activeUserId == -1)
        {
            // no active user, app starts first time or data was lost
            return getDefaultUser();
        }
        else
        {
            // try to fetch user from database
            return getUserFromDatabase( activeUserId );
        }
    }

    protected Bundle getUserFromDatabase( int userId )
    {
        UserVO existingUser = userDAO.getUserById(userId);
        if (existingUser != null)
        {
            Bundle user = new Bundle();

            // fill bundle from database
            user.putInt(FragmentSetupConstants.BUNDLE_USER_ID, existingUser.getId());
            user.putInt(FragmentSetupConstants.BUNDLE_IDENTIFIER_AGE, existingUser.getAge());
            user.putBoolean(FragmentSetupConstants.BUNDLE_IDENTIFIER_GENDER, existingUser.getMale());
            user.putFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_HEIGHT, existingUser.getHeight());
            user.putFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_WEIGHT, existingUser.getWeight());
            user.putFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_BODY_FAT, existingUser.getBodyFat());
            user.putInt(FragmentSetupConstants.BUNDLE_IDENTIFIER_ACTIVITY, existingUser.getActivity());
            user.putInt(FragmentSetupConstants.BUNDLE_IDENTIFIER_STRATEGY, existingUser.getStrategy());

            return user;
        }
        else
        {
            // user not found in database, return default template instead
            return getDefaultUser();
        }
    }

    protected Bundle getDefaultUser()
    {
        Bundle values = new Bundle();
        values.putInt(FragmentSetupConstants.BUNDLE_USER_ID, -1);
        values.putInt(FragmentSetupConstants.BUNDLE_IDENTIFIER_AGE, FragmentSetupConstants.DEFAULT_AGE);
        values.putBoolean(FragmentSetupConstants.BUNDLE_IDENTIFIER_GENDER, FragmentSetupConstants.DEFAULT_GENDER);
        values.putFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_HEIGHT, FragmentSetupConstants.DEFAULT_HEIGHT);
        values.putFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_WEIGHT, FragmentSetupConstants.DEFAULT_WEIGHT);
        values.putFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_BODY_FAT, FragmentSetupConstants.DEFAULT_BODY_FAT);
        values.putInt(FragmentSetupConstants.BUNDLE_IDENTIFIER_ACTIVITY, FragmentSetupConstants.DEFAULT_ACTIVITY);
        values.putInt(FragmentSetupConstants.BUNDLE_IDENTIFIER_STRATEGY, FragmentSetupConstants.DEFAULT_STRATEGY);
        return values;
    }

    public void onUserSetupSubmit(int tdee, int proteinGuide, int carbohydrateGuide, int fatGuide)
    {
        long activeUserId = saveDataBundle();

        // set active user
        SharedPreferences preferences = this.getSharedPreferences(StorageConstants.PREFERENCE_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        // save new active user ID and nutrition guidelines
        editor.putInt(StorageConstants.ACTIVE_USER_ID, (int) activeUserId);
        editor.putInt(StorageConstants.CURRENT_TDEE, tdee);
        editor.putInt(StorageConstants.CURRENT_DAILY_PROTEIN_GUIDE_NUMBER, proteinGuide);
        editor.putInt(StorageConstants.CURRENT_DAILY_CARBOHYDRATE_GUIDE_NUMBER, carbohydrateGuide);
        editor.putInt(StorageConstants.CURRENT_DAILY_FAT_GUIDE_NUMBER, fatGuide);
        editor.apply();

        // return to main activity
        closeActivity(activeUserId);
    }

    protected long saveDataBundle()
    {
        // save user values to DB and set is currently active user
        UserVO userToInsert = new UserVO();
        userToInsert.setId(userValues.getInt(FragmentSetupConstants.BUNDLE_USER_ID));
        userToInsert.setAge(userValues.getInt(FragmentSetupConstants.BUNDLE_IDENTIFIER_AGE));
        userToInsert.setMale(userValues.getBoolean(FragmentSetupConstants.BUNDLE_IDENTIFIER_GENDER));
        userToInsert.setHeight(userValues.getFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_HEIGHT));
        userToInsert.setWeight(userValues.getFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_WEIGHT));
        userToInsert.setBodyFat(userValues.getFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_BODY_FAT));
        userToInsert.setActivity(userValues.getInt(FragmentSetupConstants.BUNDLE_IDENTIFIER_ACTIVITY));
        userToInsert.setStrategy(userValues.getInt(FragmentSetupConstants.BUNDLE_IDENTIFIER_STRATEGY));
        return userDAO.addUser(userToInsert);
    }

    protected void closeActivity(long newUserID)
    {
        Intent i = new Intent();
        i.putExtra(StorageConstants.NEW_USER_ID, (int)newUserID);
        setResult(RESULT_OK, i);
        finish();
    }

    @Override
    public void onBackPressed()
    {
        if (pager.getCurrentItem() == 0)
        {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.

            Intent i = new Intent();
            setResult(RESULT_CANCELED, i);
            finish();
        }
        else
        {
            // Otherwise, select the previous step.
            pager.setCurrentItem(pager.getCurrentItem() - 1);
        }
    }

    private class MyPagerAdapter extends FragmentStatePagerAdapter
    {

        public MyPagerAdapter(FragmentManager fm)
        {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos)
        {
            switch (pos)
            {

                case 0:
                    return SlideWelcome.newInstance();
                case 1:
                    return SlideAge.newInstance(userValues);
                case 2:
                    return SlideGender.newInstance(userValues);
                case 3:
                    return SlideBodyHeight.newInstance(userValues);
                case 4:
                    return SlideBodyWeight.newInstance(userValues);
                case 5:
                    return SlideBodyFat.newInstance(userValues);
                case 6:
                    return SlideActivity.newInstance(userValues);
                case 7:
                    return SlideStrategy.newInstance(userValues);
                case 8:
                default:
                    return SlideResultFragment.newInstance(userValues);
            }
        }

        @Override
        public int getCount()
        {
            return 9;
        }
    }
}
