package com.mealplanning.vigilon.mealplanning.fragments;


public class FragmentSetupConstants
{
    public static final String BUNDLE_NAME = "userValues";

    public static final String BUNDLE_USER_ID = "userId";
    public static final String BUNDLE_IDENTIFIER_AGE = "age";
    public static final String BUNDLE_IDENTIFIER_HEIGHT = "height";
    public static final String BUNDLE_IDENTIFIER_WEIGHT = "weight";
    public static final String BUNDLE_IDENTIFIER_GENDER = "gender";
    public static final String BUNDLE_IDENTIFIER_BODY_FAT = "bodyFat";
    public static final String BUNDLE_IDENTIFIER_ACTIVITY = "activity";
    public static final String BUNDLE_IDENTIFIER_STRATEGY = "strategy";

    // user age in years
    public static final int DEFAULT_AGE = 30;
    static final float STEP_VALUE_AGE = 1f;
    static final float MIN_VALUE_AGE = 5f;
    static final float MAX_VALUE_AGE = 120f;

    // user height in cm
    public static final float DEFAULT_HEIGHT = 180f;
    static final float STEP_VALUE_HEIGHT = 1f;
    static final float MIN_VALUE_HEIGHT = 50f;
    static final float MAX_VALUE_HEIGHT = 230f;

    // user weight in kg
    public static final float DEFAULT_WEIGHT = 70f;
    static final float STEP_VALUE_WEIGHT = 0.1f;
    static final float MIN_VALUE_WEIGHT = 30f;
    static final float MAX_VALUE_WEIGHT = 300f;

    // user sex (true = male)
    public static final boolean DEFAULT_GENDER = true;

    // user body fat percentage
    public static final float DEFAULT_BODY_FAT = 20f;
    static final float STEP_VALUE_BODY_FAT = 0.1f;
    static final float MIN_VALUE_BODY_FAT = 0f;
    static final float MAX_VALUE_BODY_FAT = 100f;

    // user activity level index
    public static final int DEFAULT_ACTIVITY = 1;

    // user training strategy index
    public static final int DEFAULT_STRATEGY = 1;
}
