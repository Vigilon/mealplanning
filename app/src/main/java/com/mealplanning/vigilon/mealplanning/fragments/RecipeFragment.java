package com.mealplanning.vigilon.mealplanning.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.dataStructures.IngredientVO;
import com.mealplanning.vigilon.mealplanning.dataStructures.RecipeVO;
import com.mealplanning.vigilon.mealplanning.database.entities.Recipe;
import com.mealplanning.vigilon.mealplanning.database.RecipeViewModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RecipeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RecipeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RecipeFragment extends Fragment
{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String RECIPE_ID = "recipeId";

    private TabLayout tabLayout;
    private ViewPager viewPager;

    private CheckBox cb_recipe_favorite;
    private TextView tv_recipe_title;
    private TextView tv_recipe_description;

    private RecipeVO recipeVO;

    private RecipeViewModel recipeViewModel;
    private Recipe recipe;



    public RecipeFragment()
    {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment RecipeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RecipeFragment newInstance(int recipeId)
    {
        RecipeFragment fragment = new RecipeFragment();
        Bundle args = new Bundle();
        args.putInt(RECIPE_ID, recipeId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        recipeViewModel = ViewModelProviders.of(this).get(RecipeViewModel.class);
        if (getArguments() != null)
        {
            int recipeId = getArguments().getInt(RECIPE_ID);
            recipe = recipeViewModel.getRecipeById(recipeId);
        }


        recipeVO = new RecipeVO();
        recipeVO.setIngredients(new ArrayList<IngredientVO>());
        recipeVO.getIngredients().add(new IngredientVO(1, 150));
        recipeVO.getIngredients().add(new IngredientVO(2, 90));
        recipeVO.getIngredients().add(new IngredientVO(3, 90));
        recipeVO.getIngredients().add(new IngredientVO(4, 90));
        recipeVO.getIngredients().add(new IngredientVO(5, 90));
        recipeVO.getIngredients().add(new IngredientVO(6, 90));
        recipeVO.getIngredients().add(new IngredientVO(7, 90));
        recipeVO.getIngredients().add(new IngredientVO(8, 90));
        recipeVO.getIngredients().add(new IngredientVO(9, 90));
        recipeVO.getIngredients().add(new IngredientVO(10, 90));
        recipeVO.getIngredients().add(new IngredientVO(11, 90));
        recipeVO.getIngredients().add(new IngredientVO(12, 90));
        recipeVO.getIngredients().add(new IngredientVO(13, 90));
        recipeVO.getIngredients().add(new IngredientVO(14, 90));
        recipeVO.getIngredients().add(new IngredientVO(15, 90));
        recipeVO.getIngredients().add(new IngredientVO(16, 90));
        recipeVO.getIngredients().add(new IngredientVO(17, 90));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recipe, container, false);

        tv_recipe_title = view.findViewById(R.id.TV_recipe_name);
        tv_recipe_description = view.findViewById(R.id.TV_recipe_description);

        cb_recipe_favorite = view.findViewById(R.id.CB_recipe_favorite);
        cb_recipe_favorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                // mark/remove as favorite)
            }
        });

        viewPager = view.findViewById(R.id.VP_recipe_details);
        viewPager.setAdapter(new RecipeDetailsAdapter(getChildFragmentManager()));
        tabLayout = view.findViewById(R.id.TL_layout_details);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        tv_recipe_title.setText(recipe.getTitle());

        if (recipe.getDescription() != null && !recipe.getDescription().isEmpty())
        {
            tv_recipe_description.setText(recipe.getDescription());
        }
        else {
            tv_recipe_description.setVisibility(View.GONE);
        }

        cb_recipe_favorite.setChecked(recipe.isFavorite());
        cb_recipe_favorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                if (b) {
                    recipeViewModel.addToFavorites(recipe.id);
                }
                else {
                    recipeViewModel.removeFromFavorites(recipe.id);
                }
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener
    {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class RecipeDetailsAdapter extends FragmentStatePagerAdapter
    {
        RecipeDetailsAdapter(FragmentManager fm)
        {
            super(fm);
        }

        @Override
        public Fragment getItem(int position)
        {
            switch (position)
            {
                case 0:
                    return SlideRecipeIngredients.newInstance(recipe.id);
                case 1:
                    return SlideRecipePreparation.newInstance(recipe.id);
                default:
                    return SlideRecipeNutritionalValues.newInstance(recipeVO);
            }
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            switch (position)
            {
                case 0:
                    return getString(R.string.ingredients);
                case 1:
                    return getString(R.string.preparation);
                default:
                    return getString(R.string.nutritional_values);
            }
        }

        @Override
        public int getCount()
        {
            return 3;
        }
    }
}
