package com.mealplanning.vigilon.mealplanning.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.widgets.ButtonNumberPicker;

public class SlideBodyFat extends Fragment
{
    private Bundle userValues;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_slide_body_fat, container, false);

        float inputValue = FragmentSetupConstants.DEFAULT_BODY_FAT;

        // define content of fragment
        userValues = getArguments().getBundle(FragmentSetupConstants.BUNDLE_NAME);
        if (userValues != null)
        {
            inputValue = userValues.getFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_BODY_FAT, FragmentSetupConstants.DEFAULT_BODY_FAT);
        }

        ButtonNumberPicker heightPicker = (ButtonNumberPicker) v.findViewById(R.id.BTNP_body_fat);
        heightPicker.setInputValue(inputValue);
        heightPicker.setOnChangeListener(new ButtonNumberPicker.ButtonNumberPickerListener()
        {
            @Override
            public void onValueChanged(float value)
            {
                userValues.putFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_BODY_FAT, value);
            }
        });

        return v;
    }

    public static SlideBodyFat newInstance(Bundle userValues)
    {
        SlideBodyFat f = new SlideBodyFat();
        Bundle b = new Bundle();
        b.putBundle(FragmentSetupConstants.BUNDLE_NAME, userValues);
        f.setArguments(b);

        return f;
    }
}
