package com.mealplanning.vigilon.mealplanning.dataStructures;

import android.util.Log;

import com.mealplanning.vigilon.mealplanning.constants.CalculationUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DayVO
{
    // identifier
    private String _dateString; // TODO: check how to store date properly

    // guide numbers (daily goal)
    private float _carbohydrateGuideNumber;
    private float _proteinGuideNumber;
    private float _fatGuideNumber;
    private float _energyGuideNumber;

    // current values (cached to avoid recalculation)
    private float _currentProteinNumber;
    private float _currentCarbohydrateNumber;
    private float _currentFatNumber;
    private float _currentEnergyNumber;

    private List<MealVO> _mealList = new ArrayList<>();

    public DayVO()
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        _dateString = df.format(new Date());
    }

    public void initWithUser(UserVO user)
    {
        _carbohydrateGuideNumber = CalculationUtils.getDailyCarbohydrateGuideNumber(user);
        _proteinGuideNumber = CalculationUtils.getDailyProteinGuideNumber(user);
        _fatGuideNumber = CalculationUtils.getDailyFatGuideNumber(user);
        _energyGuideNumber = CalculationUtils.getTotalDailyEnergyGuideNumber(user);

        _currentCarbohydrateNumber = 0f;
        _currentProteinNumber = 0f;
        _currentFatNumber = 0f;
        _currentEnergyNumber = 0f;
    }

    public void updateWithUser(UserVO user)
    {
        _carbohydrateGuideNumber = CalculationUtils.getDailyCarbohydrateGuideNumber(user);
        _proteinGuideNumber = CalculationUtils.getDailyProteinGuideNumber(user);
        _fatGuideNumber = CalculationUtils.getDailyFatGuideNumber(user);
        _energyGuideNumber = CalculationUtils.getTotalDailyEnergyGuideNumber(user);
    }

    // region GETTER FUNCTIONS
    public float getCarbohydrateGuideNumber()
    {
        return _carbohydrateGuideNumber;
    }

    public float getProteinGuideNumber()
    {
        return _proteinGuideNumber;
    }

    public float getFatGuideNumber()
    {
        return _fatGuideNumber;
    }

    public float getEnergyGuideNumber()
    {
        return _energyGuideNumber;
    }

    public float getCurrentCarbohydrateNumber()
    {
        return _currentCarbohydrateNumber;
    }

    public float getCurrentProteinNumber()
    {
        return _currentProteinNumber;
    }

    public float getCurrentFatNumber()
    {
        return _currentFatNumber;
    }

    public float getCurrentEnergyNumber()
    {
        return _currentEnergyNumber;
    }

    public float getEnergyProgress()
    {
        return _energyGuideNumber == 0 ? 1f : _currentEnergyNumber / _energyGuideNumber;
    }

    public float getProteinProgress()
    {
        return _proteinGuideNumber == 0 ? 1f : _currentProteinNumber / _proteinGuideNumber;
    }

    public float getCarbohydrateProgress()
    {
        return _carbohydrateGuideNumber == 0 ? 1f : _currentCarbohydrateNumber / _carbohydrateGuideNumber;
    }

    public float getFatProgress()
    {
        return _fatGuideNumber == 0 ? 1f : _currentFatNumber / _fatGuideNumber;
    }

    public String getDateString()
    {
        return _dateString;
    }

    public List<MealVO> getMealList()
    {
        return _mealList;
    }

    public String getMealDBJSON()
    {
        JSONArray meals = new JSONArray();
        try
        {
            for (int i = 0; i < _mealList.size(); i++)
            {
                MealVO meal = _mealList.get(i);
                JSONArray mealObject = new JSONArray();

                List<IngredientVO> ingredients = meal.getIngredients();
                for (int j = 0; j < ingredients.size(); j++)
                {
                    IngredientVO ingredient = ingredients.get(j);
                    JSONObject ingredientObject = new JSONObject();
                    ingredientObject.put("id", ingredient.getFoodId());
                    ingredientObject.put("a", ingredient.getAmount());
                    mealObject.put(ingredientObject);
                }
                meals.put(mealObject);
            }
        } catch (JSONException e)
        {
            Log.i("JSON generation error", "Error while generating meal list: " + e);
        }

        return meals.toString();
    }
    // endregion

    // region SETTER FUNCTIONS
    public void setCarbohydrateGuideNumber(float value)
    {
        _carbohydrateGuideNumber = value;
    }

    public void setProteinGuideNumber(float value)
    {
        _proteinGuideNumber = value;
    }

    public void setFatGuideNumber(float value)
    {
        _fatGuideNumber = value;
    }

    public void setEnergyGuideNumber(float value)
    {
        _energyGuideNumber = value;
    }

    public void setDate(String value)
    {
        _dateString = value;
    }

    public void setMealList(String mealString)
    {
        _mealList = new ArrayList<>();

        try
        {
            JSONArray meals = new JSONArray(mealString);
            for (int i = 0; i < meals.length(); i++)
            {
                JSONArray ingredients = meals.getJSONArray(i);

                MealVO meal = new MealVO();
                for (int j = 0; j < ingredients.length(); j++)
                {
                    // adding all ingredients used in this meal
                    JSONObject ingredient = ingredients.getJSONObject(j);
                    meal.addIngredient(new IngredientVO(ingredient.getInt("id"), ingredient.getInt("a")));
                }
                _mealList.add(meal);

            }
        } catch (JSONException e)
        {
            Log.i("Parsing error", "Error while parsing meal list: " + e);
        }

        // recalculate values for caching
        recalculateCurrentMacronutrients();
    }
    // endregion


    protected void recalculateCurrentMacronutrients()
    {
        _currentCarbohydrateNumber = 0;
        _currentProteinNumber = 0;
        _currentFatNumber = 0;

        for (MealVO mealVO : _mealList)
        {
            _currentCarbohydrateNumber += mealVO.getTotalCarboyhdrateValue();
            _currentProteinNumber += mealVO.getTotalProteinValue();
            _currentFatNumber += mealVO.getTotalFatValue();
        }

        _currentEnergyNumber = _currentCarbohydrateNumber * CalculationUtils.getCarbohydrateBasicEnergyValue() +
                _currentProteinNumber * CalculationUtils.getProteinBasicEnergyValue() +
                _currentFatNumber * CalculationUtils.getFatBasicEnergyValue();
    }

    public void addMealToList(MealVO newMeal)
    {
        _mealList.add(newMeal);
        recalculateCurrentMacronutrients();

        // TODO persist changes in database
    }
}
