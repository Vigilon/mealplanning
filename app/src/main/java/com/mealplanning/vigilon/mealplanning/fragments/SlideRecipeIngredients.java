package com.mealplanning.vigilon.mealplanning.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.components.IngredientListAdapter;
import com.mealplanning.vigilon.mealplanning.database.RecipeViewModel;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SlideRecipeIngredients extends Fragment
{

    private static final String RECIPE_ID = "recipeId";

    private RecyclerView ingredientListView;

    private RecipeViewModel recipeViewModel;

    private List<Pair<String,String>> ingredientList;

    public SlideRecipeIngredients()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        recipeViewModel = ViewModelProviders.of(this).get(RecipeViewModel.class);

        if (getArguments() != null)
        {
            int recipeId = getArguments().getInt(RECIPE_ID);
            // grab foods from database
            // set amount and unit (based on food) and food name to a defined object
            // fit everything in a list

            ingredientList = recipeViewModel.getIngredientListForRecipeId(recipeId);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_slide_recipe_ingredients, container, false);

        ingredientListView = view.findViewById(R.id.RV_recipe_ingredients_list);
        ingredientListView.setHasFixedSize(true);
        ingredientListView.setNestedScrollingEnabled(false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(ingredientListView.getContext(), layoutManager.getOrientation());

        ingredientListView.setLayoutManager(layoutManager);
        ingredientListView.addItemDecoration(itemDecoration);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        IngredientListAdapter ingredientListAdapter = new IngredientListAdapter(ingredientList);
        ingredientListView.setAdapter(ingredientListAdapter);

        ingredientListAdapter.notifyDataSetChanged();
    }

    public static SlideRecipeIngredients newInstance(int recipeId)
    {
        SlideRecipeIngredients fragment = new SlideRecipeIngredients();
        Bundle args = new Bundle();

        args.putInt(RECIPE_ID, recipeId);

        fragment.setArguments(args);
        return fragment;
    }
}
