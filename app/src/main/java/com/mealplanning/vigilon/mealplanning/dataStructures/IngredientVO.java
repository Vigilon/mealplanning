package com.mealplanning.vigilon.mealplanning.dataStructures;

import android.os.Parcel;
import android.os.Parcelable;

public class IngredientVO implements Parcelable
{
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public IngredientVO createFromParcel(Parcel in) {
            return new IngredientVO(in);
        }

        public IngredientVO[] newArray(int size) {
            return new IngredientVO[size];
        }
    };

    private int _foodId;
    private int _amount;

    public IngredientVO(int foodId, int amount)
    {
        _foodId = foodId;
        _amount = amount;
    }

    /**
     * the identifier of the food
     *
     * @return database identifier of the used food
     */
    public int getFoodId()
    {
        return _foodId;
    }

    /**
     * amount of food
     *
     * @return amount in grams or milliliter
     */
    public int getAmount()
    {
        return _amount;
    }


    public IngredientVO(Parcel in)
    {
        _foodId = in.readInt();
        _amount = in.readInt();
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(_foodId);
        dest.writeInt(_amount);
    }
}
