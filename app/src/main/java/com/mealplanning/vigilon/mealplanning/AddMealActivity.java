package com.mealplanning.vigilon.mealplanning;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.mealplanning.vigilon.mealplanning.database.entities.Food;
import com.mealplanning.vigilon.mealplanning.database.FoodViewModel;
import com.mealplanning.vigilon.mealplanning.fragments.SelectFoodFragment;

public class AddMealActivity extends AppCompatActivity
{
    private FoodViewModel foodViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        SelectFoodFragment selectFoodFragment = new SelectFoodFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.testfragmentcontainer, selectFoodFragment).commit();
        /*setContentView(R.layout.activity_add_meal);

        foodViewModel = ViewModelProviders.of(this).get(FoodViewModel.class);

        final AutoCompleteTextView textView = findViewById(R.id.autocompleteFood);


        List<String> foodNames = foodViewModel.getAllFoodNames();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.list_item, foodNames);
        textView.setAdapter(adapter);
        textView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id)
            {
                String itemName = (String) parent.getItemAtPosition(pos);
                showFoodValues(foodViewModel.getFoodByName(itemName));

                // hide soft keyboard when item was chosen
                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (in !=null)
                {
                    in.hideSoftInputFromWindow(arg1.getApplicationWindowToken(), 0);
                }
            }
        });*/
    }

    protected void showFoodValues(Food food)
    {
        // TODO add a radar chart diagram that depicts energy distribution across macros
/*
        String foodInfoString = "Total Calories: " + Float.toString(food.getEnergyValue()) + "\n" +
                " Calories from Carbohydrates: " + Float.toString(Math.round(food.getCarbohydrateEnergyRatio() * 100)) + "%\n" +
                " Calories from Protein: " + Float.toString(Math.round(food.getProteinEnergyRatio() * 100)) + "%\n" +
                " Calories from Fat: " + Float.toString(Math.round(food.getFatEnergyRatio() * 100)) + "%\n" +
                " Salt: " + Float.toString(food.getSaltValue());

        TextView foodValueText = findViewById(R.id.food_values);
        foodValueText.setText(foodInfoString);

        TextView unitValueText = findViewById(R.id.TF_unit);
        unitValueText.setText( getString( food.getLiquid() ? R.string.unit_milliliter : R.string.unit_gram));*/
    }

    public void submitAddMeal(View view)
    {
        /*if (selectedFoodVO != null)
        {
            TextView amountText = findViewById(R.id.TF_amount);
            int amount = Integer.valueOf(amountText.getText().toString());

            float totalCalories = selectedFoodVO.getEnergyValue() * amount;
            float totalFat = selectedFoodVO.getFatValue() * amount;

            // hand over object
            Intent i = new Intent();
            i.putExtra("foodId", selectedFoodVO.getId());
            i.putExtra("amount", amount);

            // TODO: hand over directly or modify DB in sub activity and only refresh main upon return?

            setResult(RESULT_OK, i);
            finish();
        }*/
    }

    @Override
    public void onBackPressed()
    {
        Intent i = new Intent();
        setResult(RESULT_CANCELED, i);
        finish();
        super.onBackPressed();
    }
}
