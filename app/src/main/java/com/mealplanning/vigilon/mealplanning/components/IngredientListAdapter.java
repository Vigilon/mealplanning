package com.mealplanning.vigilon.mealplanning.components;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.dataStructures.IngredientVO;

import java.util.List;

public class IngredientListAdapter extends RecyclerView.Adapter<IngredientListAdapter.IngredientListViewHolder>
{
    private List<Pair<String,String>> ingredientList;

    public class IngredientListViewHolder extends RecyclerView.ViewHolder {
        public TextView ingredientName, ingredientAmount;

        public IngredientListViewHolder(View view)
        {
            super(view);
            ingredientName = view.findViewById(R.id.TV_ingredient_list_item_name);
            ingredientAmount = view.findViewById(R.id.TV_ingredient_list_item_amount);
        }
    }

    public IngredientListAdapter(List<Pair<String, String>> ingredientList)
    {
        this.ingredientList = ingredientList;
    }

    @Override
    @NonNull
    public IngredientListAdapter.IngredientListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ingredient_list_item, parent, false);

        return new IngredientListAdapter.IngredientListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull IngredientListAdapter.IngredientListViewHolder holder, int position) {
        Pair<String,String> ingredient = ingredientList.get(position);
        holder.ingredientName.setText(ingredient.first);
        holder.ingredientAmount.setText( ingredient.second);
    }

    @Override
    public int getItemCount() {
        return ingredientList.size();
    }
}