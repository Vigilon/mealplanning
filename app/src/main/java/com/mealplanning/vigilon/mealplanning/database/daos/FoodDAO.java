package com.mealplanning.vigilon.mealplanning.database.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.mealplanning.vigilon.mealplanning.database.entities.Food;

import java.util.List;

@Dao
public interface FoodDAO
{
    @Insert
    void insert(Food food);

    @Query("SELECT * FROM food_table")
    LiveData<List<Food>> getAllFoods();

    @Query("SELECT * FROM food_table WHERE id = :id")
    Food getFoodById(int id);

    @Query("SELECT * FROM food_table WHERE name = :name")
    Food getFoodByName(String name);

    @Query("SELECT name FROM food_table")
    List<String> getAllFoodNames();

    @Query("SELECT * FROM food_table WHERE name LIKE :name")
    List<Food> getFoodWithNameLike(String name);
}
