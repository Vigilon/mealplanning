package com.mealplanning.vigilon.mealplanning.database.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.mealplanning.vigilon.mealplanning.database.entities.Recipe;

import java.util.List;

@Dao
public interface RecipeDAO
{
    @Insert
    void insert(Recipe recipe);

    @Query("DELETE FROM recipe_table")
    void deleteAll();

    @Query("SELECT * FROM recipe_table")
    LiveData<List<Recipe>> getAllRecipes();

    @Query("SELECT * FROM recipe_table WHERE id = :id")
    Recipe getRecipeById(int id);

    @Query("UPDATE recipe_table SET isFavorite = :isFavorite WHERE id = :recipeId")
    void setFavoriteStateForRecipe(int recipeId, boolean isFavorite);
}
