package com.mealplanning.vigilon.mealplanning.dataStructures;

public class UserVO
{
    // user id in the database
    private int id = 0;

    // user name for readable identification
    private String name = "John Doe";

    // age in full years
    private int age = 30;

    // height in centimeters
    private Float height = 180f;

    // body weight in kilograms
    private Float weight = 80.0f;

    // body bodyFat in percent
    private Float bodyFat = 20.0f;

    // gender
    private Boolean isMale = true;

    // user activity level
    private int activity = 1;

    // user training strategy
    private int strategy = 1;


    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Float getHeight()
    {
        return height;
    }

    public void setHeight(Float height)
    {
        this.height = height;
    }

    public Float getWeight()
    {
        return weight;
    }

    public void setWeight(Float weight)
    {
        this.weight = weight;
    }

    public Float getBodyFat()
    {
        return bodyFat;
    }

    public void setBodyFat(Float bodyFat)
    {
        this.bodyFat = bodyFat;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public Boolean getMale()
    {
        return isMale;
    }

    public void setMale(Boolean male)
    {
        isMale = male;
    }

    public int getActivity()
    {
        return activity;
    }

    public void setActivity(int activity)
    {
        this.activity = activity;
    }

    public int getStrategy()
    {
        return strategy;
    }

    public void setStrategy(int strategy)
    {
        this.strategy = strategy;
    }
}
