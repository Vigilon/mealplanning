package com.mealplanning.vigilon.mealplanning.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.mealplanning.vigilon.mealplanning.R;

import java.util.ArrayList;
import java.util.List;

public class SearchableAdapter extends BaseAdapter implements Filterable
{
    private List<String> originalData;
    private List<String> filteredData;
    private LayoutInflater mInflater;
    private ItemFilter mFilter = new ItemFilter();
    private String currentSearchString;

    public SearchableAdapter(Context context, List<String> data)
    {
        this.filteredData = data;
        this.originalData = data;
        mInflater = LayoutInflater.from(context);
    }

    public int getCount()
    {
        if (filteredData == null)
        {
            return 0;
        }
        return filteredData.size();
    }

    public Object getItem(int position)
    {
        return filteredData.get(position);
    }

    public long getItemId(int position)
    {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        // A ViewHolder keeps references to children views to avoid unnecessary calls
        // to findViewById() on each row.
        ViewHolder holder;

        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        if (convertView == null)
        {
            convertView = mInflater.inflate(R.layout.list_item, null);

            // Creates a ViewHolder and store references to the two children views
            // we want to bind data to.
            holder = new ViewHolder();
            holder.text = convertView.findViewById(R.id.TV_list_item_text);

            // Bind the data efficiently with the holder.

            convertView.setTag(holder);
        }
        else
        {
            // Get the ViewHolder back to get fast access to the TextView
            // and the ImageView.
            holder = (ViewHolder) convertView.getTag();
        }

        // If weren't re-ordering this you could rely on what you set last time
        String itemString = filteredData.get(position);
        Spannable highlightString = new SpannableString(itemString);

        int start = itemString.toLowerCase().indexOf(currentSearchString.toLowerCase());
        int end = start + currentSearchString.length();

        if (start != end)
        {
            highlightString.setSpan(new StyleSpan(Typeface.BOLD), start, end , 0);
        }

        holder.text.setText(highlightString);

        return convertView;
    }

    static class ViewHolder
    {
        TextView text;
    }

    public Filter getFilter()
    {
        return mFilter;
    }

    private class ItemFilter extends Filter
    {
        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            FilterResults results = new FilterResults();

            if (originalData == null)
            {
                originalData = new ArrayList<>();
            }

            if (constraint == null || constraint.length() == 0)
            {
                results.values = originalData;
                results.count = originalData.size();
            }
            else
            {
                final String filterString = constraint.toString().toLowerCase();

                currentSearchString = filterString;

                ArrayList<String> matchValues = new ArrayList<>();

                for (String dataItem : originalData)
                {
                    if (dataItem.toLowerCase().contains(filterString))
                    {
                        matchValues.add(dataItem);
                    }
                }
                results.values = matchValues;
                results.count = matchValues.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            if (results.values != null)
            {
                filteredData = (ArrayList<String>) results.values;
            }
            else
            {
                filteredData = null;
            }
            if (results.count > 0)
            {
                notifyDataSetChanged();
            }
            else
            {
                notifyDataSetInvalidated();
            }
        }

    }
}