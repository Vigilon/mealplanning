package com.mealplanning.vigilon.mealplanning.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mealplanning.vigilon.mealplanning.dataStructures.DayVO;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.mealplanning.vigilon.mealplanning.database.DatabaseHelper.DB_COLUMN_CARB_LIMIT;
import static com.mealplanning.vigilon.mealplanning.database.DatabaseHelper.DB_COLUMN_DATE;
import static com.mealplanning.vigilon.mealplanning.database.DatabaseHelper.DB_COLUMN_ENERGY_LIMIT;
import static com.mealplanning.vigilon.mealplanning.database.DatabaseHelper.DB_COLUMN_FAT_LIMIT;
import static com.mealplanning.vigilon.mealplanning.database.DatabaseHelper.DB_COLUMN_MEAL_LIST;
import static com.mealplanning.vigilon.mealplanning.database.DatabaseHelper.DB_COLUMN_PROTEIN_LIMIT;

public class DayDAO
{
    private SQLiteDatabase database;

    // TODO days have no reference to user ID, might be problematic with multiple users
    private String[] allColumns = {DB_COLUMN_DATE, DB_COLUMN_CARB_LIMIT, DB_COLUMN_PROTEIN_LIMIT, DB_COLUMN_FAT_LIMIT, DB_COLUMN_ENERGY_LIMIT, DB_COLUMN_MEAL_LIST};

    public DayDAO(Context context) {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(context);
        database = dbHelper.getDatabase();
    }

    public void addDay(DayVO day)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DB_COLUMN_DATE, day.getDateString());
        contentValues.put(DB_COLUMN_CARB_LIMIT, day.getCarbohydrateGuideNumber());
        contentValues.put(DB_COLUMN_PROTEIN_LIMIT, day.getProteinGuideNumber());
        contentValues.put(DB_COLUMN_FAT_LIMIT, day.getFatGuideNumber());
        contentValues.put(DB_COLUMN_ENERGY_LIMIT, day.getEnergyGuideNumber());
        contentValues.put(DB_COLUMN_MEAL_LIST, day.getMealDBJSON());

        Log.i(this.toString() + " - insertDay", "Inserting: day" + day.getDateString());
        database.insert(DatabaseHelper.DB_TABLE_DAY, null, contentValues);
    }

    public DayVO getDayByDate(String date)
    {
        Cursor cursor = database.query(DatabaseHelper.DB_TABLE_DAY,
                allColumns, DB_COLUMN_DATE + " = '" + date + "'", null,
                null, null, null);
        cursor.moveToFirst();
        DayVO day = cursorToDay(cursor);
        cursor.close();
        return day;
    }

    public DayVO getCurrentDay()
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        // TODO auto creation of new day object?
        return getDayByDate(df.format(new Date()));
    }

    private DayVO cursorToDay(Cursor cursor) {
        if (cursor.moveToFirst())
        {
            DayVO day = new DayVO();
            day.setDate(cursor.getString(0));
            day.setCarbohydrateGuideNumber( cursor.getFloat(1));
            day.setProteinGuideNumber( cursor.getFloat( 2));
            day.setFatGuideNumber( cursor.getFloat(3));
            day.setEnergyGuideNumber( cursor.getFloat(4));
            day.setMealList( cursor.getString(5));
            return day;
        }
        else
        {
            return null;
        }
    }

    public Boolean updateDay( DayVO day )
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DB_COLUMN_DATE, day.getDateString());
        contentValues.put(DB_COLUMN_CARB_LIMIT, day.getCarbohydrateGuideNumber());
        contentValues.put(DB_COLUMN_PROTEIN_LIMIT, day.getProteinGuideNumber());
        contentValues.put(DB_COLUMN_FAT_LIMIT, day.getFatGuideNumber());
        contentValues.put(DB_COLUMN_ENERGY_LIMIT, day.getEnergyGuideNumber());
        contentValues.put(DB_COLUMN_MEAL_LIST, day.getMealDBJSON());

        Log.i(this.toString() + " - updateDay", "Updating: day" + day.getDateString());
        int affectedRows = database.update(DatabaseHelper.DB_TABLE_DAY, contentValues, DB_COLUMN_DATE + " = ?", new String[] {day.getDateString()});

        return affectedRows > 0;
    }
}
