package com.mealplanning.vigilon.mealplanning.dataStructures;

import com.mealplanning.vigilon.mealplanning.constants.CalculationUtils;

public class FoodVO
{

    private int id;
    private String name;
    private float carbohydrateValue = 0f;
    private float proteinValue = 0f;
    private float fatValue = 0f;
    private float saltValue = 0f;
    private boolean isLiquid = false;

    private float energyValue = 0f;
    private float carbohydrateEnergyRatio = 1f;
    private float proteinEnergyRatio = 1f;
    private float fatEnergyRatio = 1f;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setMacronutrients(float carbohydrate, float protein, float fat)
    {
        carbohydrateValue = carbohydrate;
        proteinValue = protein;
        fatValue = fat;

        float energyFromCarbohydrate = carbohydrateValue * CalculationUtils.getCarbohydrateBasicEnergyValue();
        float energyFromProtein = proteinValue * CalculationUtils.getProteinBasicEnergyValue();
        float energyFromFat = fatValue * CalculationUtils.getFatBasicEnergyValue();
        energyValue = energyFromCarbohydrate + energyFromProtein + energyFromFat;

        carbohydrateEnergyRatio = carbohydrateValue * CalculationUtils.getCarbohydrateBasicEnergyValue() / energyValue;
        proteinEnergyRatio = proteinValue * CalculationUtils.getProteinBasicEnergyValue() / energyValue;
        fatEnergyRatio = fatValue * CalculationUtils.getFatBasicEnergyValue() / energyValue;
    }

    public void setSaltValue(float value)
    {
        saltValue = value;
    }

    public void setIsLiquid(boolean value) { isLiquid = value; }

    public float getCarbohydrateValue()
    {
        return carbohydrateValue;
    }

    public float getProteinValue()
    {
        return proteinValue;
    }

    public float getFatValue()
    {
        return fatValue;
    }

    public float getSaltValue()
    {
        return saltValue;
    }

    public boolean getIsLiquid() { return isLiquid; }

    /**
     * energy in kcal per 100g
     *
     * @return total energy value
     */
    public float getEnergyValue()
    {
        return energyValue;
    }

    public float getCarbohydrateEnergyRatio()
    {
        return carbohydrateEnergyRatio;
    }

    public float getProteinEnergyRatio()
    {
        return proteinEnergyRatio;
    }

    public float getFatEnergyRatio()
    {
        return fatEnergyRatio;
    }
}
