package com.mealplanning.vigilon.mealplanning.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mealplanning.vigilon.mealplanning.R;

public class SlideWelcome extends Fragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_slide_welcome, container, false);

        // define content of fragment
        TextView headlineText = (TextView) v.findViewById(R.id.TF_slideGenderHeadline);
        headlineText.setText( getString(R.string.setup_welcome));

        TextView descriptionText = (TextView) v.findViewById(R.id.TF_slideTextDescription);
        descriptionText.setText( getString( R.string.setup_welcome_desc));

        return v;
    }

    public static SlideWelcome newInstance()
    {
        return new SlideWelcome();
    }
}
