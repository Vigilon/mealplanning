package com.mealplanning.vigilon.mealplanning.fragments;


import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.constants.StorageConstants;

public class SlideStrategy extends Fragment
{
    private Bundle userValues;

    private RadioButton rb_bulking;
    private RadioButton rb_cutting;
    private RadioButton rb_sustain;
    private TextView tv_strategy_details;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_slide_strategy, container, false);

        rb_bulking = (RadioButton) v.findViewById(R.id.RB_bulking);
        rb_cutting = (RadioButton) v.findViewById(R.id.RB_cutting);
        rb_sustain = (RadioButton) v.findViewById(R.id.RB_sustain);

        tv_strategy_details = (TextView) v.findViewById(R.id.TV_strategy_details);

        RadioGroup radioGroup = (RadioGroup) v.findViewById(R.id.RG_strategy);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId)
            {
                int strategyIndex = StorageConstants.STRATEGY_SUSTAIN;
                if (checkedId == rb_bulking.getId())
                {
                    strategyIndex = StorageConstants.STRATEGY_BULKING;
                }
                else if (checkedId == rb_cutting.getId())
                {
                    strategyIndex = StorageConstants.STRATEGY_CUTTING;
                }

                storeValue(strategyIndex);
                setStrategy(strategyIndex);
            }
        });


        // define content of fragment
        userValues = getArguments().getBundle(FragmentSetupConstants.BUNDLE_NAME);
        int inputValue = StorageConstants.STRATEGY_SUSTAIN;
        if (userValues != null)
        {
            inputValue = userValues.getInt(FragmentSetupConstants.BUNDLE_IDENTIFIER_STRATEGY, StorageConstants.STRATEGY_SUSTAIN);
        }

        // select radio button depending on stored value
        switch (inputValue)
        {
            case StorageConstants.STRATEGY_BULKING:
                radioGroup.check(rb_bulking.getId());
                break;
            case StorageConstants.STRATEGY_CUTTING:
                radioGroup.check(rb_cutting.getId());
                break;
            default:
                radioGroup.check(rb_sustain.getId());
        }

        setStrategy(inputValue);

        return v;
    }

    public static SlideStrategy newInstance(Bundle userValues)
    {
        SlideStrategy f = new SlideStrategy();
        Bundle b = new Bundle();
        b.putBundle(FragmentSetupConstants.BUNDLE_NAME, userValues);
        f.setArguments(b);

        return f;
    }

    protected void setStrategy(int strategyIndex)
    {
        switch (strategyIndex)
        {
            case StorageConstants.STRATEGY_BULKING:
                tv_strategy_details.setText(getString(R.string.strategy_bulking_description));
                break;
            case StorageConstants.STRATEGY_CUTTING:
                tv_strategy_details.setText(getString(R.string.strategy_cutting_description));
                break;
            default:
                tv_strategy_details.setText(getString(R.string.strategy_sustain_description));
        }
    }

    protected void storeValue(int strategyIndex)
    {
        userValues.putInt(FragmentSetupConstants.BUNDLE_IDENTIFIER_STRATEGY, strategyIndex);
    }
}
