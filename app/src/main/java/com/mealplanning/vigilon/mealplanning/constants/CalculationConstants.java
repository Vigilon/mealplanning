package com.mealplanning.vigilon.mealplanning.constants;

public class CalculationConstants
{
    public static final int ACTIVITY_NO_TRAINING = 0;
    public static final int ACTIVITY_LOW_TRAINING = 1;
    public static final int ACTIVITY_MEDIUM_TRAINING = 2;
    public static final int ACTIVITY_HIGH_TRAINING = 3;
}
