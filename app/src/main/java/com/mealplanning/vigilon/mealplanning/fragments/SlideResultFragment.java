package com.mealplanning.vigilon.mealplanning.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mealplanning.vigilon.mealplanning.constants.CalculationUtils;
import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.UserSetupActivity;
import com.mealplanning.vigilon.mealplanning.constants.BulkingStrategy;
import com.mealplanning.vigilon.mealplanning.constants.CuttingStrategy;
import com.mealplanning.vigilon.mealplanning.constants.ITrainingStrategy;
import com.mealplanning.vigilon.mealplanning.constants.StorageConstants;
import com.mealplanning.vigilon.mealplanning.constants.SustainStrategy;

import java.util.Locale;

public class SlideResultFragment extends Fragment
{
    private TextView tf_bmr;
    private TextView tf_tdee;
    private TextView tf_carb;
    private TextView tf_protein;
    private TextView tf_fat;

    private float totalEnergyExpenditure;
    private float dailyCalories;
    private int dailyProtein;
    private int dailyCarbohydrate;
    private int dailyFat;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_slide_result, container, false);

        // define content of fragment
        TextView headlineText = (TextView) v.findViewById(R.id.TF_slideResultHeadline);
        headlineText.setText(getString(R.string.result));

        TextView descriptionText = (TextView) v.findViewById(R.id.TF_slideResultDescription);
        descriptionText.setText(getString(R.string.bmr));

        // Button to commit all the gathered data to the main activity
        Button submitBtn = (Button) v.findViewById(R.id.BTN_submit);
        submitBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // trigger save and close in the fragment activity
                onSubmit();
            }
        });

        tf_bmr = (TextView) v.findViewById(R.id.TF_slideBmr);
        tf_tdee = (TextView) v.findViewById(R.id.TF_slideTdee);
        tf_carb = (TextView) v.findViewById(R.id.TF_slideCarbohydrate);
        tf_protein = (TextView) v.findViewById(R.id.TF_slideProtein);
        tf_fat = (TextView) v.findViewById(R.id.TF_slideFat);

        if(getUserVisibleHint()){ // fragment is visible
            calculateValues();
        }

        return v;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) { // fragment is visible and have created
            calculateValues();
        }
    }

    public static SlideResultFragment newInstance(Bundle setupValues)
    {
        SlideResultFragment f = new SlideResultFragment();
        Bundle b = new Bundle();
        b.putBundle(FragmentSetupConstants.BUNDLE_NAME, setupValues);
        f.setArguments(b);
        return f;
    }

    protected void calculateValues()
    {
        Bundle userValues = getArguments().getBundle(FragmentSetupConstants.BUNDLE_NAME);

        // retrieve all stored values
        float age = userValues.getInt(FragmentSetupConstants.BUNDLE_IDENTIFIER_AGE);
        boolean isMale = userValues.getBoolean(FragmentSetupConstants.BUNDLE_IDENTIFIER_GENDER);
        float bodyHeight = userValues.getFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_HEIGHT);
        float bodyWeight = userValues.getFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_WEIGHT);
        float bodyFat = userValues.getFloat(FragmentSetupConstants.BUNDLE_IDENTIFIER_BODY_FAT);
        int activityLevel = userValues.getInt(FragmentSetupConstants.BUNDLE_IDENTIFIER_ACTIVITY);
        int strategy = userValues.getInt(FragmentSetupConstants.BUNDLE_IDENTIFIER_STRATEGY);

        // determine training strategy
        ITrainingStrategy strategyToApply;
        if (strategy == StorageConstants.STRATEGY_SUSTAIN)
        {
            strategyToApply = new SustainStrategy();
        }
        else if (strategy == StorageConstants.STRATEGY_BULKING)
        {
            strategyToApply = new BulkingStrategy();
        }
        else
        {
            strategyToApply = new CuttingStrategy();
        }

        float activityRate = CalculationUtils.getActivityRate( activityLevel );

        float basalMetabolicRate = CalculationUtils.getBasalMetabolicRate(bodyHeight, bodyWeight, age, CalculationUtils.getLeanBodyMass(bodyWeight, bodyFat), isMale);

        // calculate values that have to be set
        totalEnergyExpenditure = basalMetabolicRate * activityRate;
        dailyCalories = totalEnergyExpenditure * strategyToApply.getEnergyModifier();
        dailyProtein = CalculationUtils.getDailyProteinGuideNumber(totalEnergyExpenditure, strategyToApply);
        dailyCarbohydrate = CalculationUtils.getDailyCarbohydrateGuideNumber(totalEnergyExpenditure, strategyToApply);
        dailyFat = CalculationUtils.getDailyFatGuideNumber(totalEnergyExpenditure, strategyToApply);


        tf_bmr.setText(getString(R.string.bmr_result, String.format(Locale.US, "%d", ((int) basalMetabolicRate)), getString(R.string.unit_kiloCalories)));
        tf_tdee.setText(getString(R.string.tdee_result, String.format(Locale.US, "%d", ((int) dailyCalories)), getString(R.string.unit_kiloCalories)));
        tf_carb.setText(getString(R.string.daily_sum_carbs) + " " + Integer.toString(dailyCarbohydrate));
        tf_protein.setText(getString(R.string.daily_sum_protein) + " " + Integer.toString(dailyProtein));
        tf_fat.setText(getString(R.string.daily_sum_fat) + " " + Integer.toString(dailyFat));
    }

    protected void onSubmit()
    {
        // Notify main activity
        ((UserSetupActivity) getActivity()).onUserSetupSubmit(((int) dailyCalories), dailyProtein, dailyCarbohydrate, dailyFat);
    }
}
