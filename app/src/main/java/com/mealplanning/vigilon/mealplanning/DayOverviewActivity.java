package com.mealplanning.vigilon.mealplanning;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.mealplanning.vigilon.mealplanning.components.MealListAdapter;
import com.mealplanning.vigilon.mealplanning.dataStructures.DayVO;
import com.mealplanning.vigilon.mealplanning.dataStructures.MealVO;
import com.mealplanning.vigilon.mealplanning.database.DayDAO;

import java.util.ArrayList;

public class DayOverviewActivity extends AppCompatActivity
{

    protected DayDAO dayDAO;
    protected DayVO dayVO;

    ListView simpleList;
    ArrayList<MealVO> mealList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_overview);

        dayDAO = new DayDAO(this);

        setUpDayMealList();
    }

    protected void setUpDayMealList()
    {
        dayVO = dayDAO.getCurrentDay();

        //setContentView(R.layout.activity_main);
        simpleList = findViewById(R.id.LV_dayFood);

        //mealList = (ArrayList) dayVO.getMealList();

        MealListAdapter myAdapter = new MealListAdapter(this,R.layout.meal_list_item , mealList);
        simpleList.setAdapter(myAdapter);
    }
}
