package com.mealplanning.vigilon.mealplanning.fragments;


import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mealplanning.vigilon.mealplanning.R;

public class SlideGender extends Fragment
{
    private Bundle userValues;

    private RadioButton rb_male;
    private RadioButton rb_female;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_slide_gender, container, false);

        userValues = getArguments().getBundle(FragmentSetupConstants.BUNDLE_NAME);

        rb_male = (RadioButton) v.findViewById(R.id.RB_male);
        rb_female = (RadioButton) v.findViewById(R.id.RB_female);

        RadioGroup radioGroup = (RadioGroup) v.findViewById(R.id.RG_gender);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId)
            {
                if (checkedId == rb_male.getId())
                {
                    storeValue(true);
                }
                else if (checkedId == rb_female.getId())
                {
                    storeValue(false);
                }
            }
        });

        Boolean isMale = userValues.getBoolean(FragmentSetupConstants.BUNDLE_IDENTIFIER_GENDER, true);

        if (isMale)
        {
            rb_male.setChecked(true);
        }
        else
        {
            rb_female.setChecked(true);
        }
        return v;
    }

    public static SlideGender newInstance(Bundle userValues)
    {
        SlideGender f = new SlideGender();
        Bundle b = new Bundle();
        b.putBundle(FragmentSetupConstants.BUNDLE_NAME, userValues);
        f.setArguments(b);

        return f;
    }

    protected void storeValue(Boolean isMale)
    {
        userValues.putBoolean(FragmentSetupConstants.BUNDLE_IDENTIFIER_GENDER, isMale);
    }
}