package com.mealplanning.vigilon.mealplanning.constants;

import com.mealplanning.vigilon.mealplanning.dataStructures.UserVO;

public class CalculationUtils
{
    public static float getActivityRate(int index)
    {
        switch (index)
        {
            case CalculationConstants.ACTIVITY_NO_TRAINING:
                return 1f;
            case CalculationConstants.ACTIVITY_LOW_TRAINING:
                return 1.2f;
            case CalculationConstants.ACTIVITY_MEDIUM_TRAINING:
                return 1.35f;
            case CalculationConstants.ACTIVITY_HIGH_TRAINING:
            default:
                return 1.5f;
        }
    }

    public static int getBasalMetabolicRate(float heightInCm, float weightInKg, float age, float leanBodyMassInKg, boolean isMale)
    {
        // after Mifflin-StJeor
        float bmr1 = getBasicMetabolicRateMifflinStJeor(heightInCm, weightInKg, age, isMale);

        // after Katch-McArdle formula
        float bmr2 = getBasicMetabolicRateKatchMcArdle(leanBodyMassInKg);

        return Math.round((bmr1 + bmr2) * 0.5f);
    }

    public static float getLeanBodyMass(float weightInKg, float bodyFatInPercent)
    {
        return (weightInKg * (100 - bodyFatInPercent)) / 100;
    }

    private static float getBasicMetabolicRateMifflinStJeor(float heightInCm, float weightInKg, float age, boolean isMale)
    {
        float bmr;
        if (isMale)
        {
            bmr = 10 * weightInKg + 6.25f * heightInCm - 5 * age + 5;
        }
        else
        {
            bmr = 10 * weightInKg + 6.25f * heightInCm - 5 * age - 161;
        }
        return bmr;
    }

    private static float getBasicMetabolicRateKatchMcArdle(float leanBodyMass)
    {
        return 370 + (21.6f * leanBodyMass);
    }

    public static int getTotalDailyEnergyGuideNumber(UserVO user)
    {
        return Math.round(getDailyCarbohydrateGuideNumber(user) * getCarbohydrateBasicEnergyValue() + getDailyProteinGuideNumber(user) * getProteinBasicEnergyValue() + getDailyFatGuideNumber(user) * getFatBasicEnergyValue());
    }

    public static int getDailyProteinGuideNumber(float tdee, ITrainingStrategy appliedStrategy)
    {
        return (int) ((tdee * appliedStrategy.getEnergyModifier() * appliedStrategy.getProteinPercentage()) / getProteinBasicEnergyValue());
    }

    public static int getDailyProteinGuideNumber(UserVO user)
    {
        float bmr = getBasalMetabolicRate(user.getHeight(), user.getWeight(), user.getAge(), getLeanBodyMass(user.getWeight(), user.getBodyFat()), user.getMale());

        // determine training strategy
        ITrainingStrategy strategyToApply;
        if (user.getStrategy() == 0)
        {
            strategyToApply = new SustainStrategy();
        }
        else if (user.getStrategy() == 1)
        {
            strategyToApply = new BulkingStrategy();
        }
        else
        {
            strategyToApply = new CuttingStrategy();
        }

        float tdee = bmr * getActivityRate(user.getActivity());

        return getDailyProteinGuideNumber(tdee, strategyToApply);
    }

    public static int getDailyCarbohydrateGuideNumber(float tdee, ITrainingStrategy appliedStrategy)
    {
        return (int) ((tdee * appliedStrategy.getEnergyModifier() * appliedStrategy.getCarbohydratePercentage()) / getCarbohydrateBasicEnergyValue());
    }

    public static int getDailyCarbohydrateGuideNumber(UserVO user)
    {
        float bmr = getBasalMetabolicRate(user.getHeight(), user.getWeight(), user.getAge(), getLeanBodyMass(user.getWeight(), user.getBodyFat()), user.getMale());

        // determine training strategy
        ITrainingStrategy strategyToApply;
        if (user.getStrategy() == 0)
        {
            strategyToApply = new SustainStrategy();
        }
        else if (user.getStrategy() == 1)
        {
            strategyToApply = new BulkingStrategy();
        }
        else
        {
            strategyToApply = new CuttingStrategy();
        }

        float tdee = bmr * getActivityRate(user.getActivity());

        return getDailyCarbohydrateGuideNumber(tdee, strategyToApply);
    }

    public static int getDailyFatGuideNumber(float tdee, ITrainingStrategy appliedStrategy)
    {
        return (int) ((tdee * appliedStrategy.getEnergyModifier() * appliedStrategy.getFatPercentage()) / getFatBasicEnergyValue());
    }

    public static int getDailyFatGuideNumber(UserVO user)
    {
        float bmr = getBasalMetabolicRate(user.getHeight(), user.getWeight(), user.getAge(), getLeanBodyMass(user.getWeight(), user.getBodyFat()), user.getMale());

        // determine training strategy
        ITrainingStrategy strategyToApply;

        // TODO think about storing all strategies in an enum
        if (user.getStrategy() == 0)
        {
            strategyToApply = new SustainStrategy();
        }
        else if (user.getStrategy() == 1)
        {
            strategyToApply = new BulkingStrategy();
        }
        else
        {
            strategyToApply = new CuttingStrategy();
        }

        float tdee = bmr * getActivityRate(user.getActivity());

        return getDailyFatGuideNumber(tdee, strategyToApply);
    }

    // energy value of a single gram of protein
    public static float getProteinBasicEnergyValue()
    {
        return 4.1f;
    }

    // energy value of a single gram of carbohydrate
    public static float getCarbohydrateBasicEnergyValue()
    {
        return 4.1f;
    }

    // energy value of a single gram of fat
    public static float getFatBasicEnergyValue()
    {
        return 9f;
    }

    public static float getEnergyValue(float carbohydrateValue, float proteinValue, float fatValue)
    {
        return carbohydrateValue * getCarbohydrateBasicEnergyValue() + proteinValue * getProteinBasicEnergyValue() + fatValue * getFatBasicEnergyValue();
    }
}
