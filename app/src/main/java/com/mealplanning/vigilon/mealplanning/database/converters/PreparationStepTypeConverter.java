package com.mealplanning.vigilon.mealplanning.database.converters;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class PreparationStepTypeConverter
{
    @TypeConverter
    public static List<String> toPreparationStepList(String preparationStepsString)
    {
        if (preparationStepsString.isEmpty())
        {
            return new ArrayList<>();
        }
        else
        {
            Type listType = new TypeToken<ArrayList<String>>()
            {
            }.getType();
            return new Gson().fromJson(preparationStepsString, listType);
        }
    }

    @TypeConverter
    public static String toString(List<String> preparationSteps)
    {
        if (preparationSteps == null || preparationSteps.isEmpty())
        {
            return "";
        }
        else
        {
            return new Gson().toJson(preparationSteps);
        }
    }
}
