package com.mealplanning.vigilon.mealplanning.constants;

public class SustainStrategy implements ITrainingStrategy
{
    @Override
    public int getStrategyIndex()
    {
        return StorageConstants.STRATEGY_SUSTAIN;
    }

    @Override
    public float getProteinPercentage()
    {
        return 0.15f;
    }

    @Override
    public float getCarbohydratePercentage()
    {
        return 0.55f;
    }

    @Override
    public float getFatPercentage()
    {
        return 0.3f;
    }

    @Override
    public float getEnergyModifier(){ return 1f; }
}
