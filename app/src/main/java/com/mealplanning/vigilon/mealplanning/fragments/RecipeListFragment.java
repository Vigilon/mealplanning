package com.mealplanning.vigilon.mealplanning.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mealplanning.vigilon.mealplanning.MealPlanning;
import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.components.RecipeListAdapter;
import com.mealplanning.vigilon.mealplanning.database.entities.Recipe;
import com.mealplanning.vigilon.mealplanning.database.RecipeViewModel;

import java.util.List;

public class RecipeListFragment extends Fragment
{
    private TextView recipeListHeader;
    private RecyclerView recipeListView;
    private RecipeListAdapter recipeAdapter;

    private RecipeViewModel mRecipeViewModel;

    public RecipeListFragment()
    {
        // Required empty public constructor
    }

    public static RecipeListFragment newInstance()
    {
        RecipeListFragment fragment = new RecipeListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Observer here instead of in onCreate() to get observer back once fragment was detached
        mRecipeViewModel = ViewModelProviders.of(this).get(RecipeViewModel.class);
        mRecipeViewModel.getAllRecipes().observe(this, new Observer<List<Recipe>>() {
            @Override
            public void onChanged(@Nullable final List<Recipe> recipes) {
                // Update the cached copy of the recipes in the adapter.
                recipeAdapter.setRecipes(recipes);

                recipeListHeader.setText(getString(R.string.placeholder_recipes_found, recipes != null ? recipes.size() : 0));
            }
        });

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recipe_list, container, false);
        recipeListHeader = view.findViewById(R.id.TV_recipe_list_header);

        recipeListView = view.findViewById(R.id.RV_recipe_list);
        recipeListView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        DividerItemDecoration itemDecoration = new DividerItemDecoration(recipeListView.getContext(), layoutManager.getOrientation());

        recipeListView.setLayoutManager(layoutManager);
        recipeListView.addItemDecoration(itemDecoration);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        recipeAdapter = new RecipeListAdapter(getContext());
        recipeAdapter.setClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int position = recipeListView.indexOfChild(v);
                // TODO try to improve this!!
                showRecipe(mRecipeViewModel.getAllRecipes().getValue().get(position).id);
            }
        });

        recipeListView.setAdapter(recipeAdapter);
    }

    public void onResume(){
        super.onResume();

        // Set title bar
        ((MealPlanning) getActivity()).setActionBarTitle(getString(R.string.recipes));

    }

    protected void showRecipe(int recipeId)
    {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_meal_planning, RecipeFragment.newInstance(recipeId)).addToBackStack(null).commit();
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        mRecipeViewModel.getAllRecipes().removeObservers(this);
    }
}
