package com.mealplanning.vigilon.mealplanning.widgets;

import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.view.View;

import com.mealplanning.vigilon.mealplanning.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MacroDistributionChart extends View
{
    private RectF mOval = new RectF();
    private Paint mTextPaint;
    private Paint mShadowPaint;

    private Paint mCarbohydratePaint;
    private Paint mProteinPaint;
    private Paint mFatPaint;

    private float mTextHeight = 48f;

    private static final float ANGLE_GAP = 2f;

    private float strokeWidth = 50f;

    private List<Float> mData;
    private int calories;

    private int mWidth = 600;
    private int mHeight = 600;

    public MacroDistributionChart(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        /*TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CircleProgress,
                0, 0);*/

        init();
    }

    private void init()
    {
        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setColor(Color.BLACK);
        if (mTextHeight == 0)
        {
            mTextHeight = mTextPaint.getTextSize();
        }
        else
        {
            mTextPaint.setTextSize(mTextHeight);
        }
        mTextPaint.setStyle(Paint.Style.FILL);

        // set color properties
        mCarbohydratePaint = new Paint();
        mCarbohydratePaint.setColor(ResourcesCompat.getColor(getResources(), R.color.colorSignatureCarbohydrate, null));
        mCarbohydratePaint.setStrokeWidth(strokeWidth);
        mCarbohydratePaint.setStyle(Paint.Style.STROKE);
        mCarbohydratePaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        // set color properties
        mProteinPaint = new Paint();
        mProteinPaint.setColor(ResourcesCompat.getColor(getResources(), R.color.colorSignatureProtein, null));
        mProteinPaint.setStrokeWidth(strokeWidth);
        mProteinPaint.setStyle(Paint.Style.STROKE);
        mProteinPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        // set color properties
        mFatPaint = new Paint();
        mFatPaint.setColor(ResourcesCompat.getColor(getResources(), R.color.colorSignatureFat, null));
        mFatPaint.setStrokeWidth(strokeWidth);
        mFatPaint.setStyle(Paint.Style.STROKE);
        mFatPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        mShadowPaint = new Paint(0);
        mShadowPaint.setColor(0xff101010);
        mShadowPaint.setMaskFilter(new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL));
    }

    public void setMacros(float carbohydratePercentage, float proteinPercentage, float fatPercentage, int calories)
    {
        mData = new ArrayList<>();
        mData.add(carbohydratePercentage);
        mData.add(proteinPercentage);
        mData.add(fatPercentage);

        this.calories = calories;

        this.invalidate();
    }

    private void drawText(Canvas canvas, Paint paint, String text, Paint percentagePaint)
    {
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        Rect percentageBounds = new Rect();
        percentagePaint.getTextBounds("%", 0, 1, percentageBounds);
        int x = ((int)mOval.width() / 2) - (bounds.width() / 2) - (percentageBounds.width() / 2);
        int y = ((int)mOval.height() / 2) + (bounds.height() / 2);
        canvas.drawText(text, x, y, paint);

        canvas.drawText("kcal", x + bounds.width() + percentageBounds.width() / 2, y - bounds.height() + percentageBounds.height(), percentagePaint);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        // carbohydrate percentage start at -90 = top
        float startAngle = -90f;

        mOval.set(strokeWidth, strokeWidth, mWidth - strokeWidth, mHeight - strokeWidth);

        drawText(canvas, mTextPaint, Integer.toString(calories), mTextPaint);

        for (int i = 0; i < mData.size(); ++i)
        {
            float percentage = mData.get(i);
            if (percentage != 0f)
            {
                float sweepAngle = Math.max((percentage * 360f) - ANGLE_GAP, 1);
                switch (i)
                {
                    case 0:
                        canvas.drawArc(mOval, startAngle, sweepAngle, false, mCarbohydratePaint);
                        break;
                    case 1:
                        canvas.drawArc(mOval, startAngle, sweepAngle, false, mProteinPaint);
                        break;
                    default:
                        canvas.drawArc(mOval, startAngle, sweepAngle, false, mFatPaint);
                        break;
                }

                float x = mWidth / 2 + (mOval.width() / 2) * (float) Math.cos(Math.toRadians(startAngle + sweepAngle / 2));
                float y = mWidth / 2 + (mOval.width() / 2) * (float) Math.sin(Math.toRadians(startAngle + sweepAngle / 2));
                canvas.drawCircle(x, y, 5, mShadowPaint);
                canvas.drawText(String.format(Locale.US,"%.1f", percentage * 100) + "%", x, y, mTextPaint);

                startAngle += sweepAngle + ANGLE_GAP;
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int desiredWidth = mWidth;
        int desiredHeight = mHeight;

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(desiredWidth, widthSize);
        } else {
            //Be whatever you want
            width = desiredWidth;
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(desiredHeight, heightSize);
        } else {
            //Be whatever you want
            height = desiredHeight;
        }

        //MUST CALL THIS
        setMeasuredDimension(width, height);
    }

    /*@Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        // Account for padding
        float xpad = (float)(getPaddingLeft() + getPaddingRight());
        float ypad = (float)(getPaddingTop() + getPaddingBottom());

        // Account for the label
        if (mShowText) xpad += mTextWidth;

        float ww = (float)w - xpad;
        float hh = (float)h - ypad;

        // Figure out how big we can make the pie.
        float diameter = Math.min(ww, hh);

        super.onSizeChanged(ww, );
    }*/
}
