package com.mealplanning.vigilon.mealplanning.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mealplanning.vigilon.mealplanning.dataStructures.UserVO;


/**
 * DB access object for information about the user such as:
 * body weight, height, gender, food preferences etc
 */
public class UserDAO
{
    private SQLiteDatabase database;

    private String[] allColumns = {DatabaseHelper.DB_COLUMN_ID, DatabaseHelper.DB_COLUMN_USER_NAME, DatabaseHelper.DB_COLUMN_BODY_WEIGHT, DatabaseHelper.DB_COLUMN_BODY_HEIGHT, DatabaseHelper.DB_COLUMN_BODY_FAT, DatabaseHelper.DB_COLUMN_AGE, DatabaseHelper.DB_COLUMN_GENDER, DatabaseHelper.DB_COLUMN_ACTIVITY, DatabaseHelper.DB_COLUMN_STRATEGY};

    public UserDAO(Context context) {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(context);
        database = dbHelper.getDatabase();
    }

    public long addUser(UserVO user)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.DB_COLUMN_USER_NAME, user.getName());
        contentValues.put(DatabaseHelper.DB_COLUMN_BODY_WEIGHT, user.getWeight());
        contentValues.put(DatabaseHelper.DB_COLUMN_BODY_HEIGHT, user.getHeight());
        contentValues.put(DatabaseHelper.DB_COLUMN_BODY_FAT, user.getBodyFat());
        contentValues.put(DatabaseHelper.DB_COLUMN_AGE, user.getAge());
        contentValues.put(DatabaseHelper.DB_COLUMN_GENDER, user.getMale() ? 1 : 0);
        contentValues.put(DatabaseHelper.DB_COLUMN_ACTIVITY, user.getActivity());
        contentValues.put(DatabaseHelper.DB_COLUMN_STRATEGY, user.getStrategy());

        Log.i(this.toString() + " - insertUser", "Inserting: " + user.getName());
        //database.insert(DatabaseHelper.DB_TABLE_USER, null, contentValues);
        return database.insertWithOnConflict(DatabaseHelper.DB_TABLE_USER, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public void updateUser(UserVO user)
    {
        ContentValues contentValues = new ContentValues();
       // database.update(DatabaseHelper.DB_TABLE_USER, contentValues, DatabaseHelper.DB_COLUMN_ID, user.getId());
    }

    public UserVO getUserById(int userId) {
        Cursor cursor = database.query(DatabaseHelper.DB_TABLE_USER,
                allColumns, DatabaseHelper.DB_COLUMN_ID + " = '" + userId + "'", null,
                null, null, null);
        if (cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            UserVO user = cursorToUser(cursor);
            cursor.close();
            return user;
        }
        else
        {
            return null;
        }
    }

    private UserVO cursorToUser(Cursor cursor) {
        UserVO user = new UserVO();
        user.setId(cursor.getInt(0));
        user.setName(cursor.getString(1));
        user.setWeight(cursor.getFloat(2));
        user.setHeight(cursor.getFloat(3));
        user.setBodyFat(cursor.getFloat(4));
        user.setAge(cursor.getInt(5));
        user.setMale(cursor.getInt(6) == 1);
        user.setActivity(cursor.getInt(7));
        user.setStrategy(cursor.getInt(8));
        return user;
    }

    public String[] getAllUsers() {
        Cursor cursor = database.query(DatabaseHelper.DB_TABLE_USER, new String[]{DatabaseHelper.DB_COLUMN_USER_NAME}, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            String[] str = new String[cursor.getCount()];
            int i = 0;

            while (cursor.moveToNext()) {
                str[i] = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DB_COLUMN_USER_NAME));
                i++;
            }
            return str;
        } else {
            return new String[]{};
        }
    }
}
