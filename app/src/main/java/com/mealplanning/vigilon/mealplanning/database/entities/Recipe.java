package com.mealplanning.vigilon.mealplanning.database.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.mealplanning.vigilon.mealplanning.dataStructures.IngredientVO;
import com.mealplanning.vigilon.mealplanning.database.converters.IngredientTypeConverter;
import com.mealplanning.vigilon.mealplanning.database.converters.PreparationStepTypeConverter;

import java.util.List;

@Entity(tableName = "recipe_table")
@TypeConverters({IngredientTypeConverter.class, PreparationStepTypeConverter.class})
public class Recipe
{
    @PrimaryKey(autoGenerate = true)
    public int id;

    @NonNull
    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo
    private String description;

    @ColumnInfo
    private int preparationTime;

    @ColumnInfo
    private List<String> preparationSteps;

    @ColumnInfo
    private List<IngredientVO> ingredients;

    @ColumnInfo
    private float proteinValue;

    @ColumnInfo
    private float fatValue;

    @ColumnInfo
    private float carbohydrateValue;

    @ColumnInfo
    private boolean isFavorite;

    public Recipe(@NonNull String title)
    {
        this.title = title;
    }

    @NonNull
    public String getTitle()
    {
        return this.title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public int getPreparationTime()
    {
        return preparationTime;
    }

    public void setPreparationTime(int preparationTime)
    {
        this.preparationTime = preparationTime;
    }

    public List<String> getPreparationSteps() { return preparationSteps; }

    public void setPreparationSteps(List<String> preparationSteps) {
        this.preparationSteps = preparationSteps;
    }

    public List<IngredientVO> getIngredients()
    {
        return ingredients;
    }

    public void setIngredients(List<IngredientVO> ingredients)
    {
        this.ingredients = ingredients;
    }

    public void setProteinValue(float value) {
        this.proteinValue = value;
    }

    public float getProteinValue(){
        return this.proteinValue;
    }

    public float getFatValue()
    {
        return fatValue;
    }

    public void setFatValue(float fatValue)
    {
        this.fatValue = fatValue;
    }

    public float getCarbohydrateValue()
    {
        return carbohydrateValue;
    }

    public void setCarbohydrateValue(float carbohydrateValue)
    {
        this.carbohydrateValue = carbohydrateValue;
    }

    public boolean isFavorite()
    {
        return isFavorite;
    }

    public void setFavorite(boolean favorite)
    {
        isFavorite = favorite;
    }
}
