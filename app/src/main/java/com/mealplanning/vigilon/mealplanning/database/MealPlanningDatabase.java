package com.mealplanning.vigilon.mealplanning.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.mealplanning.vigilon.mealplanning.dataStructures.IngredientVO;
import com.mealplanning.vigilon.mealplanning.database.converters.IngredientTypeConverter;
import com.mealplanning.vigilon.mealplanning.database.daos.FoodDAO;
import com.mealplanning.vigilon.mealplanning.database.daos.RecipeDAO;
import com.mealplanning.vigilon.mealplanning.database.entities.Food;
import com.mealplanning.vigilon.mealplanning.database.entities.Recipe;

import java.util.ArrayList;
import java.util.List;

@Database(entities = {Recipe.class, Food.class}, version = 1)
public abstract class MealPlanningDatabase extends RoomDatabase
{
    private static MealPlanningDatabase INSTANCE;

    public static MealPlanningDatabase getDatabase(final Context context)
    {
        if (INSTANCE == null)
        {
            synchronized (MealPlanningDatabase.class)
            {
                if (INSTANCE == null)
                {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MealPlanningDatabase.class, "mealPlanning_database")
                            .addCallback(sRoomDatabaseCallback)
                            .addMigrations(MIGRATION_1_2)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback()
            {

                @Override
                public void onCreate(@NonNull SupportSQLiteDatabase db)
                {
                    super.onCreate(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void>
    {
        private final RecipeDAO mRecipeDao;
        private final FoodDAO mFoodDao;

        PopulateDbAsync(MealPlanningDatabase db)
        {
            mRecipeDao = db.recipeDAO();
            mFoodDao = db.foodDAO();
        }

        @Override
        protected Void doInBackground(final Void... params)
        {
            // enter dummy recipes
            Recipe recipe = new Recipe("Chia-Pudding im Glas");
            recipe.setProteinValue(0.5f);
            recipe.setPreparationTime(5);
            List<IngredientVO> ingredients = new ArrayList<>();
            ingredients.add(new IngredientVO(1, 150));
            ingredients.add(new IngredientVO(2, 90));
            recipe.setIngredients(ingredients);

            List<String> preparationSteps = new ArrayList<>();
            preparationSteps.add("Chiasamen, Quinoa, Amaranth, Haferflocken, Zimt  und Leinsamen sowie Rosinen und Cashews in ein Einweckglas geben.");
            preparationSteps.add("Die Milch hinzufügen und gut verrühren. Je nachdem wie fest oder flüssig man seinen Pudding mag, mehr oder weniger Milch nehmen.");
            preparationSteps.add("Das Gläschen für mindestens eine Stunde, am besten aber über Nacht, im Kühlschrank quellen lassen.");
            preparationSteps.add("Wer mag, kann vor dem Verzehr noch ein paar frische Beeren oder anderes Obst drüber geben.");
            recipe.setPreparationSteps(preparationSteps);
            mRecipeDao.insert(recipe);
            recipe = new Recipe("Gulasch");
            mRecipeDao.insert(recipe);
            recipe = new Recipe("Spaghetti Bolognese");
            mRecipeDao.insert(recipe);

            // enter dummy food
            populateFood();
            return null;
        }

        private void populateFood()
        {
            Food food = new Food("Milch (1,5%)", 0.05f, 0.034f, 0.015f, 0);
            food.setVegan(false);
            food.setVegetarian(true);
            food.setLiquid(true);
            food.generateValues();
            mFoodDao.insert(food);

            food = new Food("Hähnchen", 0f, 0.223f, 0.015f, 0f);
            food.setVegan(false);
            food.setVegetarian(false);
            food.setLiquid(false);
            food.generateValues();
            mFoodDao.insert(food);

            food = new Food("Banane", 0.2f, 0.01f, 0.002f, 0f);
            food.setVegan(true);
            food.setVegetarian(true);
            food.setLiquid(false);
            food.generateValues();
            mFoodDao.insert(food);

            food = new Food("Kartoffel", 0.14f, 0.02f, 0f, 0f);
            food.setVegan(true);
            food.setVegetarian(true);
            food.setLiquid(false);
            food.generateValues();
            mFoodDao.insert(food);
        }
    }

    // test migration
    private static final Migration MIGRATION_1_2 = new Migration(1, 2)
    {
        @Override
        public void migrate(SupportSQLiteDatabase database)
        {
            // Since we didn't alter the table, there's nothing else to do here.
        }
    };

    public abstract RecipeDAO recipeDAO();

    public abstract FoodDAO foodDAO();
}
