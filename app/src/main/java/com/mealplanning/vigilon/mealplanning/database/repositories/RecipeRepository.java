package com.mealplanning.vigilon.mealplanning.database.repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.mealplanning.vigilon.mealplanning.database.MealPlanningDatabase;
import com.mealplanning.vigilon.mealplanning.database.daos.RecipeDAO;
import com.mealplanning.vigilon.mealplanning.database.entities.Recipe;

import java.util.List;

public class RecipeRepository
{
    private RecipeDAO mRecipeDao;
    private LiveData<List<Recipe>> mAllRecipes;

    public RecipeRepository(Application application)
    {
        MealPlanningDatabase db = MealPlanningDatabase.getDatabase(application);
        mRecipeDao = db.recipeDAO();
        mAllRecipes = mRecipeDao.getAllRecipes();
    }

    public LiveData<List<Recipe>> getAllRecipes()
    {
        return mAllRecipes;
    }

    public Recipe getRecipeById(int id)
    {
        Recipe recipe;
        try {
            recipe = new selectRecipeAsyncTask(mRecipeDao).execute(id).get();
        }
        catch (Exception e)
        {
            recipe = null;
        }
        return recipe;
    }

    private static class selectRecipeAsyncTask extends AsyncTask<Integer, Void, Recipe>
    {
        private RecipeDAO mAsyncTaskDao;

        selectRecipeAsyncTask(RecipeDAO dao)
        {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Recipe doInBackground(final Integer... params)
        {
            return mAsyncTaskDao.getRecipeById(params[0]);
        }
    }

    public void insert(Recipe recipe)
    {
        new insertAsyncTask(mRecipeDao).execute(recipe);
    }

    private static class insertAsyncTask extends AsyncTask<Recipe, Void, Void>
    {
        private RecipeDAO mAsyncTaskDao;

        insertAsyncTask(RecipeDAO dao)
        {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Recipe... params)
        {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public void setFavoriteStateForRecipe(int recipeId, boolean isFavorite) {
        new updateFavoriteAsyncTask(mRecipeDao, recipeId, isFavorite).execute();
    }

    private static class updateFavoriteAsyncTask extends AsyncTask<Void, Void, Void>
    {
        private RecipeDAO mAsyncTaskDao;
        private int mRecipeId;
        private boolean mIsFavorite;

        updateFavoriteAsyncTask(RecipeDAO dao, int recipeId, boolean isFavorite)
        {
            mAsyncTaskDao = dao;
            mRecipeId = recipeId;
            mIsFavorite = isFavorite;
        }

        @Override
        protected Void doInBackground(Void ... params)
        {
            mAsyncTaskDao.setFavoriteStateForRecipe(mRecipeId, mIsFavorite);
            return null;
        }
    }
}