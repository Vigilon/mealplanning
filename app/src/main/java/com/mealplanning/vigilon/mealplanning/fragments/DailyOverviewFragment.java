package com.mealplanning.vigilon.mealplanning.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mealplanning.vigilon.mealplanning.MealPlanning;
import com.mealplanning.vigilon.mealplanning.R;
import com.mealplanning.vigilon.mealplanning.dataStructures.DayVO;
import com.mealplanning.vigilon.mealplanning.widgets.CircleProgress;

/**
 * A simple {@link Fragment} subclass.
 */
public class DailyOverviewFragment extends Fragment
{
    protected CircleProgress circleProgress;

    protected TextView tf_energy;
    protected TextView tf_protein;
    protected TextView tf_carbohydrate;
    protected TextView tf_fat;

    DayVO dayVO;

    public DailyOverviewFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_daily_overview, container, false);
        tf_energy = view.findViewById(R.id.TF_calories_sum);
        tf_protein = view.findViewById(R.id.TF_protein);
        tf_carbohydrate = view.findViewById(R.id.TF_carbs);
        tf_fat = view.findViewById(R.id.TF_fat);
        circleProgress = view.findViewById(R.id.progress_circle);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        if (dayVO != null)
        {
            updateDailyMacronutrients(dayVO);
        }
    }

    public void onResume(){
        super.onResume();

        // Set title bar
        ((MealPlanning) getActivity()).setActionBarTitle(getString(R.string.daily_overview));
    }

    public void setDayVO(DayVO dayVO)
    {
        this.dayVO = dayVO;
    }

    /**
     * sets the current daily macronutritial values and limits
     */
    public void updateDailyMacronutrients(DayVO day)
    {
        // total daily energy expenditure
        tf_energy.setText(getString(R.string.general_fraction, (int) day.getCurrentEnergyNumber(), (int) day.getEnergyGuideNumber()));

        // protein values
        tf_protein.setText(getString(R.string.general_fraction, (int) day.getCurrentProteinNumber(), (int) day.getProteinGuideNumber()));

        // carbohydrate values
        tf_carbohydrate.setText(getString(R.string.general_fraction, (int) day.getCurrentCarbohydrateNumber(), (int) day.getCarbohydrateGuideNumber()));

        // fat values
        tf_fat.setText(getString(R.string.general_fraction, (int) day.getCurrentFatNumber(), (int) day.getFatGuideNumber()));

        // update circle progress for test purposes^^
        circleProgress.setMaxValue(day.getCarbohydrateGuideNumber());
        circleProgress.setProgress(day.getCarbohydrateProgress());
        circleProgress.startAnimation();
    }
}
