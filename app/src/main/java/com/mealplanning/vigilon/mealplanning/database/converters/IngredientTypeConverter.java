package com.mealplanning.vigilon.mealplanning.database.converters;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mealplanning.vigilon.mealplanning.dataStructures.IngredientVO;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class IngredientTypeConverter
{
    @TypeConverter
    public static List<IngredientVO> toIngredientList(String ingredientString)
    {
        Type listType = new TypeToken<ArrayList<IngredientVO>>(){}.getType();
        return new Gson().fromJson(ingredientString, listType);
    }

    @TypeConverter
    public static String toString(List<IngredientVO> ingredientList)
    {
        return new Gson().toJson(ingredientList);
    }
}
