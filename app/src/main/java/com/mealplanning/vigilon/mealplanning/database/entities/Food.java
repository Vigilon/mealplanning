package com.mealplanning.vigilon.mealplanning.database.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.mealplanning.vigilon.mealplanning.constants.CalculationUtils;

@Entity(tableName = "food_table")
public class Food
{
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo
    private String name;

    @ColumnInfo
    private float carbohydrateValue;

    @ColumnInfo
    private float proteinValue;

    @ColumnInfo
    private float fatValue;

    @ColumnInfo
    private float saltValue;

    @ColumnInfo(name = "liquid")
    private boolean isLiquid = false;

    @ColumnInfo(name = "vegetarian")
    private boolean isVegetarian;

    @ColumnInfo(name = "vegan)")
    private boolean isVegan;

    @ColumnInfo
    private float energyValue;

    @ColumnInfo
    private float carbohydrateEnergyRatio;

    @ColumnInfo
    private float proteinEnergyRatio;

    @ColumnInfo
    private float fatEnergyRatio;

    public Food(@NonNull String name, float carbohydrateValue, float proteinValue, float fatValue, float saltValue)
    {
        this.name = name;
        this.carbohydrateValue = carbohydrateValue;
        this.proteinValue = proteinValue;
        this.fatValue = fatValue;
        this.saltValue = saltValue;
    }

    public int getId()
    {
        return this.id;
    }

    @NonNull
    public String getName()
    {
        return this.name;
    }

    public float getCarbohydrateValue()
    {
        return this.carbohydrateValue;
    }

    public float getProteinValue()
    {
        return this.proteinValue;
    }

    public float getFatValue()
    {
        return this.fatValue;
    }

    public float getSaltValue()
    {
        return this.saltValue;
    }

    public void setLiquid(boolean isLiquid)
    {
        this.isLiquid = isLiquid;
    }

    public boolean getLiquid() {
        return this.isLiquid;
    }

    public void setVegetarian(boolean isVegetarian)
    {
        this.isVegetarian = isVegetarian;
    }

    public boolean getVegetarian(){
        return this.isVegetarian;
    }

    public void setVegan(boolean isVegan)
    {
        this.isVegan = isVegan;
    }

    public boolean getVegan(){
        return this.isVegan;
    }

    public void setEnergyValue(float energyValue)
    {
        this.energyValue = energyValue;
    }

    public float getEnergyValue(){
        return this.energyValue;
    }

    public float getCarbohydrateEnergyRatio()
    {
        return carbohydrateEnergyRatio;
    }

    public void setCarbohydrateEnergyRatio(float carbohydrateEnergyRatio)
    {
        this.carbohydrateEnergyRatio = carbohydrateEnergyRatio;
    }

    public float getProteinEnergyRatio()
    {
        return proteinEnergyRatio;
    }

    public void setProteinEnergyRatio(float proteinEnergyRatio)
    {
        this.proteinEnergyRatio = proteinEnergyRatio;
    }

    public float getFatEnergyRatio()
    {
        return fatEnergyRatio;
    }

    public void setFatEnergyRatio(float fatEnergyRatio)
    {
        this.fatEnergyRatio = fatEnergyRatio;
    }

    public void generateValues()
    {
        energyValue = CalculationUtils.getEnergyValue(carbohydrateValue, proteinValue, fatValue);

        carbohydrateEnergyRatio = (carbohydrateValue * CalculationUtils.getCarbohydrateBasicEnergyValue()) / energyValue;
        proteinEnergyRatio = (proteinValue * CalculationUtils.getProteinBasicEnergyValue()) / energyValue;
        fatEnergyRatio = (fatValue * CalculationUtils.getFatBasicEnergyValue()) / energyValue;
    }
}
