package com.mealplanning.vigilon.mealplanning.widgets;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.mealplanning.vigilon.mealplanning.R;

public class CircleProgress extends View
{

    private final RectF mOval = new RectF();
    private float mSweepAngle = 0;
    private int startAngle = 90;
    private int angleGap = 2;
    private Bitmap icon;

    private float currentValue = 1f;
    private float maxValue = 1f;

    float mEndAngle = 1.0f;

    Paint progressPaint = new Paint();
    TextPaint textPaint = new TextPaint();
    Paint incompletePaint = new Paint();
    Paint percentagePaint = new Paint();
    Paint exceedPaint = new Paint();

    private float strokeWidth = 30.0f;

    // animation duration in milliseconds
    private int animationDuration = 2000;

    public CircleProgress(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CircleProgress,
                0, 0);

        int textColor;
        float textSize;

        int progressColor;
        int incompleteColor;
        int exceedColor;

        try
        {
            textColor = a.getColor(R.styleable.CircleProgress_android_textColor, getResources().getColor(android.R.color.holo_red_dark));
            textSize = a.getDimension(R.styleable.CircleProgress_android_textSize, 100);

            strokeWidth = a.getDimension(R.styleable.CircleProgress_strokeWidth, 30.0f);

            progressColor = a.getColor(R.styleable.CircleProgress_progressColor, getResources().getColor(android.R.color.holo_blue_bright));
            incompleteColor = a.getColor(R.styleable.CircleProgress_incompleteProgressColor, getResources().getColor(android.R.color.darker_gray));
            exceedColor = a.getColor(R.styleable.CircleProgress_exceedColor, getResources().getColor(android.R.color.holo_red_dark));

        } finally
        {
            a.recycle();
        }

        // set color properties
        progressPaint.setColor(progressColor);
        progressPaint.setStrokeWidth(strokeWidth);
        progressPaint.setStyle(Paint.Style.STROKE);
        progressPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        incompletePaint.setColor(incompleteColor);
        incompletePaint.setStrokeWidth(strokeWidth);
        incompletePaint.setStyle(Paint.Style.STROKE);
        incompletePaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        exceedPaint.setColor(exceedColor);
        exceedPaint.setStrokeWidth(strokeWidth);
        exceedPaint.setStyle(Paint.Style.STROKE);
        exceedPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        textPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        textPaint.setColor(textColor);
        textPaint.setTextSize(textSize);
        Typeface tf = Typeface.create("Roboto Condensed Light", Typeface.BOLD);
        textPaint.setTypeface(tf);

        percentagePaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        percentagePaint.setColor(textColor);
        percentagePaint.setTextSize(textSize / 3);


        icon = BitmapFactory.decodeResource(getResources(), R.drawable.icon_carbs);

    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        // draw icon first in the background in the very center
        if (icon != null)
        {
            canvas.drawBitmap(icon, canvas.getWidth() / 2 - icon.getWidth() / 2, canvas.getHeight() / 2 - icon.getHeight() / 2, null);
        }

        float top = getHeight() * 0.5f - icon.getHeight() * 0.5f;
        float bottom = getHeight() * 0.5f + icon.getHeight() * 0.5f;
        float left = getWidth() * 0.5f - icon.getWidth() * 0.5f;
        float right = getWidth() * 0.5f + icon.getWidth() * 0.5f;

        float currentAngleGap = mSweepAngle == 1.0f || mSweepAngle == 0 ? 0 : angleGap;

        // set progress circle ( 0 - 1 ) - wrap around the icon
        mOval.set(left - strokeWidth / 2, top - strokeWidth / 2, right + strokeWidth / 2, bottom + strokeWidth / 2);
        canvas.drawArc(mOval, -startAngle + currentAngleGap, (mSweepAngle * 360) - currentAngleGap, false, progressPaint);

        if (mEndAngle <= 1)
        {
            // set leftover circle ( 0 - 1)
            mOval.set(left - strokeWidth / 2, top - strokeWidth / 2, right + (strokeWidth / 2), bottom + (strokeWidth / 2));
            canvas.drawArc(mOval, mSweepAngle * 360 - startAngle + currentAngleGap, 360 - (mSweepAngle * 360) - currentAngleGap, false,
                    incompletePaint);
        }
        else
        {
            mOval.set(left - strokeWidth / 2, top -strokeWidth / 2, right + (strokeWidth / 2), bottom + (strokeWidth / 2));
            canvas.drawArc(mOval, -startAngle + currentAngleGap, ((mSweepAngle - 1) * 360) - currentAngleGap, false, exceedPaint);
        }


        drawText(canvas, textPaint, String.valueOf((int) (mSweepAngle * 100)), percentagePaint);

        //drawText(canvas, textPaint, getContext().getString(R.string.general_fraction_short, (int) currentValue, (int) maxValue), percentagePaint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    private void drawText(Canvas canvas, Paint paint, String text, Paint percentagePaint)
    {
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        Rect percentageBounds = new Rect();
        percentagePaint.getTextBounds("%", 0, 1, percentageBounds);
        int x = (canvas.getWidth() / 2) - (bounds.width() / 2) - (percentageBounds.width() / 2);
        int y = (canvas.getHeight() / 2) + (bounds.height() / 2);
        canvas.drawText(text, x, y, paint);

        canvas.drawText("kcal", x + bounds.width() + percentageBounds.width() / 2, y - bounds.height() + percentageBounds.height(), percentagePaint);
    }

    public void setTextColor(int color)
    {
        textPaint.setColor(color);
    }

    public void setProgressColor(int color)
    {
        progressPaint.setColor(color);
    }

    public void setIncompleteColor(int color)
    {
        incompletePaint.setColor(color);
    }

    public void setCurrentValue(float currentValue)
    {
        this.currentValue = currentValue;

        mEndAngle = currentValue / maxValue;
        this.invalidate();
    }

    public void setMaxValue(float maxValue)
    {
        this.maxValue = maxValue;
    }

    public void setProgress(float progress)
    {
        if (progress < 0)
        {
            throw new RuntimeException("Value must be between 0 and 1: " + progress);
        }


        //mEndAngle = progress > 1.0f ? progress - 1f : progress;
        mEndAngle = progress;

        this.invalidate();
    }

    public void startAnimation()
    {
        ValueAnimator anim = ValueAnimator.ofFloat(mSweepAngle, mEndAngle);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
        {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator)
            {
                CircleProgress.this.mSweepAngle = (Float) valueAnimator.getAnimatedValue();
                CircleProgress.this.invalidate();
            }
        });
        anim.setDuration(animationDuration);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.start();
    }
}