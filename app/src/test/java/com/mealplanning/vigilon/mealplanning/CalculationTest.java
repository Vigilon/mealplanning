package com.mealplanning.vigilon.mealplanning;

import android.widget.TextView;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import static junit.framework.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class)
public class CalculationTest
{
    @Test
    public void basicTest()
    {
        Assert.assertEquals(4, 2 + 2);
    }
}
